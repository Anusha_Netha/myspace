import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Techsupport from '../src/Components/Techsupport';
import SuccessfullProject from '../src/Components/SuccessfullProjects';
import Profile from '../src/Components/Profile1';
import LoginPage from './Components/loginform';
import Home from './Components/Home';
import Rave from './Components/Rave';
import Postrave from './Components/postrave';
import drive from './Components/drive';
import Appraisal from './Components/Appraisal';
import postAppraisal from './Components/postAppraisal';
import Holidays from './Components/holidays';
import QueryForumHome from './Components/QueryForumHome';
import Search from './Components/Search';
import SearchDrillDown from './Components/Example';

//this class contains all the routes for the whole application
class Routes extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path={`/`} component={LoginPage} exact />
                    <Route path={`/home`} component={Home} exact />
                    <Route path={`/search/:value`} component={Search} />
                    <Route path={`/profile`} component={Profile} exact />
                    <Route path={`/techsupport`} component={Techsupport} exact />
                    <Route path={`/successfullproject`} component={SuccessfullProject} exact />
                    <Route path={`/raveall`} component={Rave} />
                    <Route path={`/postrave`} component={Postrave} />
                    <Route path={`/drives`} component={drive} />
                    <Route path={`/appraisal`} component={Appraisal} />
                    <Route path={`/postappraisal`} component={postAppraisal} />
                    <Route path={`/holidays`} component={Holidays} />
                    <Route path={`/queryForum`} component={QueryForumHome} />
                    <Route path={`/example`} component={SearchDrillDown}/>
                </Switch>
            </BrowserRouter>
        );
    }
}

export default Routes;