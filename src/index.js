import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider} from 'react-redux';
import configureStore from './Store/Utilities/configureStore'
import * as serviceWorker from './serviceWorker';
const storeInstance = configureStore();

ReactDOM.render(
<Provider store={storeInstance}>
<App />
</Provider>,document.getElementById('root'));


serviceWorker.unregister();