import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from '../Reducers/rootreducer';
import LoginService from '../Middlewares/loginservice';
import ProfileService from '../Middlewares/profileservice';
import SearchService from '../Middlewares/searchservice';
import RaveService from '../Middlewares/raveservice';
import PostRaveService from '../Middlewares/PostRaveService';
import DriveService from '../Middlewares/driveservice';
import HomeService from '../Middlewares/HomeService';
import AppraisalService from '../Middlewares/fetchappraisalservice';
import postAppraisalService from '../Middlewares/Postappraisalservice';
import searchNameService from '../Middlewares/searchbynameservice';
import UpdateProfileServcie from '../Middlewares/updateprofileservice';
import QueryForumService from '../Middlewares/queryforumservice';
import PostQuestionService from '../Middlewares/postQuestionService';
import FetchAnswersByIdService from '../Middlewares/fetchAnswersByIdService';
import PostAnswerservice from '../Middlewares/postanswerservice';
import DeleteQuestionService from '../Middlewares/deleteQuestionService';
import DeleteAnswerService from '../Middlewares/deleteAnswerService';
import EditAnswerservice from '../Middlewares/editAnswerService';
import GetPhotoService from '../Middlewares/getPhotoService';

//creates a store and combines services
export default function configureStore() {
    return createStore(
        rootReducer,
        compose(applyMiddleware(
            LoginService,
            ProfileService,
            SearchService,
            RaveService,
            PostRaveService,
            DriveService,
            HomeService,
            AppraisalService,
            postAppraisalService,
            searchNameService,
            UpdateProfileServcie,
            QueryForumService,
            PostQuestionService,
            FetchAnswersByIdService,
            PostAnswerservice,
            DeleteQuestionService,
            DeleteAnswerService,
            EditAnswerservice,
            GetPhotoService,
        ))
    );
};