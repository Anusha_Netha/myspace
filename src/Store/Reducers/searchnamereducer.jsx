import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    details: {},
    statuscode: 0,
    isLoaded: false
}

//gets the information of an employee based on their name
export default function searchnamereducer(state = initialState, action) {

    switch (action.type) {

        case allActions.SEARCH_EMPLOYEE_BY_NAME:
            return action;

        case allActions.RECEIEVE_EMPLOYEE_BY_NAME:
            debugger;
            return {
                ...state,
                isLoaded: true,
                details: action.payload.result,
                statuscode: action.payload.statuscode,
            }
        case allActions.SEARCH_NOT_EXISTS:
            debugger;
            return {
                ...state,
                statuscode: action.payload,
            }

        default:
            return state;
    }
}