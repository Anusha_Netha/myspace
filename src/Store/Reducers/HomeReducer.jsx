import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    notification: [],
    statuscode: '',
    isLoaded: false
}

//gets the notification
export default function homereducer(state = initialState, action) {

    switch (action.type) {

        case allActions.NOTIFICATION:
            return action;

        case allActions.RECEIVE_NOTIFICATION:

            return {
                ...state,
                isLoaded: true,
                notification: action.payload.Notification,
                statuscode: action.payload.status
            }

        default:
            return state;
    }
}