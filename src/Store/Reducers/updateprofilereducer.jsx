import * as allActions from '../Actions/actionConstant';

//intialises variables
const initialState = {
    details: {},
    statuscode: 0,
    isLoaded: false
}

//updates the employee information based on id
export default function updateprofilereducer(state = initialState, action) {

    switch (action.type) {

        case allActions.UPDATE_VALUES:
            return action;

        case allActions.UPDATED_SUCCESSFULL:

            return {
                ...state,
                isLoaded: true,
                details: action.payload.updated_employee_details,
                statuscode: action.payload.status
            }
        case allActions.UPDATE_ERROR:

            return {
                ...state,
                isLoaded: true,
                statuscode: action.payload.statuscode
            }
        default:
            return state;
    }
}