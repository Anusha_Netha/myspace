import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    statuscode: 0,
    isLoaded: false
}

//deletes a question from database
export default function deletequestionreducer(state = initialState, action) {

    switch (action.type) {

        case allActions.DELETE_QUESTION:
            return action;

        case allActions.DELETE_QUESTION_SUCCESS:

            return {
                ...state,
                isLoaded: true,
                statuscode: action.payload.statuscode
            }
        case allActions.DELETE_QUESTION_ERROR:
            return {
                ...state,
                isLoaded: false,
                statuscode: action.payload.statuscode
            }
        default:
            return state;
    }
}