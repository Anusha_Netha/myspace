import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    data: 0,
    isLoaded: false
}

//posts a rave
export default function postravereducer(state = initialState, action) {

    switch (action.type) {

        case allActions.POSTRAVE:
            return action;

        case allActions.POSTRAVESUCCESS:

            return {
                ...state,
                isLoaded: true,
                data: action.payload.statuscode
            }
        case allActions.POST_RAVE_DATA_ERROR:
            return {
                ...state,
                isLoaded: false,
                data: action.payload.statuscode
            }
        default:
            return state;
    }
}