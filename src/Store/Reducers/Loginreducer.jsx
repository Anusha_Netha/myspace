import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    is_logged_in: false,
    statusCode: 0

}

//login reducer to get the details of a user
export default function loginreducer(state = initialState, action) {
    console.log('login reducer')
    switch (action.type) {

        case allActions.DO_LOGIN_USER:
            console.log('do login user')
            return action;

        case allActions.LOGIN_USER_SUCCESS:
            console.log('login user success');
            return {

                ...state,
                is_logged_in: true,
                statusCode: action.payload
            }

        case allActions.LOGIN_USER_DATA_ERROR:
            console.log('login error');
            return {
                is_logged_in: false,
                statusCode: action.payload
            }

        default:
            return state;
    }
}