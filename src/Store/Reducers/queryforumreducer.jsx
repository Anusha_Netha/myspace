import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    questions: [],
    statuscode: '',
}

//gets all questions from the database
export default function queryForumreducer(state = initialState, action) {

    switch (action.type) {

        case allActions.GET_ALL_QUESTIONS:
            debugger;
            return action;

        case allActions.RECEIVED_QUESTIONS:
            debugger;
            return {
                ...state,
                questions: action.payload.Questions_list,
                statuscode: action.payload.statuscode,
            }

        default:
            return state;
    }
}