import * as allActions from '../Actions/actionConstant';

//intialises variables
const initialState = {
    statuscode: 0,
    isLoaded: false
}

//deletes an answer from database
export default function deleteanswerreducer(state = initialState, action) {

    switch (action.type) {

        case allActions.DELETE_ANSWER:
            return action;

        case allActions.DELETE_ANSWER_SUCCESS:

            return {
                ...state,
                isLoaded: true,
                statuscode: action.payload.statuscode
            }
        case allActions.DELETE_ANSWER_ERROR:
            return {
                ...state,
                isLoaded: false,
                statuscode: action.payload.statuscode
            }
        default:
            return state;
    }
}