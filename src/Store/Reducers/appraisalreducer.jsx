import * as allActions from '../Actions/actionConstant';

//initialises values
const initialState = {
    details: [],
    statuscode: '',
    isLoaded: false
}

//gets the appraisal data from database
export default function appraisalreducer(state = initialState, action) {

    switch (action.type) {

        case allActions.APPRAISAL_ALL:
            debugger;
            return action;

        case allActions.RECEIVE_APPRAISAL_ALL:
            debugger;
            return {
                ...state,
                isLoaded: true,
                details: action.payload.appraisal,
                statuscode: action.payload.statuscode,
            }

        default:
            return state;
    }
}