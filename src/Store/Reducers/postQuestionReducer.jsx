import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    data: 0,
    isLoaded: false
}

//posts a question
export default function postquestionreducer(state = initialState, action) {

    switch (action.type) {

        case allActions.POST_QUESTION:
            return action;

        case allActions.POST_QUESTION_SUCCESS:

            return {
                ...state,
                isLoaded: true,
                data: action.payload.statuscode
            }
        case allActions.POST_QUESTION_ERROR:
            return {
                ...state,
                isLoaded: false,
                data: action.payload.statuscode
            }
        default:
            return state;
    }
}