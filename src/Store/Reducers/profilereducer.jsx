import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    details: {},
    image: null,
    statuscode:'',
    isLoaded: false,
    status: 0
}

//gets the information of an employee based on id
export default function profilereducer(state = initialState, action) {

    switch (action.type) {

        case allActions.FETCH_PROFILE_BY_ID:
            return action;

        case allActions.RECEIEVE_PROFILE_DETAILS_BY_ID:

            return {
                ...state,
                isLoaded: true,
                details: action.payload.employee,
                statuscode:action.payload.statuscode
            }
            case allActions.PROFILE_ERROR:
            return{
                ...state,
                isLoaded : false,
                statuscode : action.payload.statuscode
            }
        default:
            return state;
    }
}