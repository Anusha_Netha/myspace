import { combineReducers } from 'redux';
import LoginReducer from './Loginreducer';
import profilereducer from './profilereducer';
import searchreducer from './searchreducer';
import ravereducer from './ravereducer';
import postravereducer from './PostRaveReducer';
import drivereducer from './drivereducer';
import homereducer from './HomeReducer';
import appraisalreducer from './appraisalreducer';
import postappraisalreducer from './postappraisalreducer';
import searchnamereducer from './searchnamereducer';
import updateprofilereducer from './updateprofilereducer';
import queryforumreducer from './queryforumreducer';
import postquestionreducer from './postQuestionReducer';
import fetchAnswerByIdreducer from './fetchAnswersByIdreducer';
import postanswerreducer from './postanswerreducer';
import deletequestionreducer from './deleteQuestionReducer';
import deleteanswerreducer from './deleteAnswerReducer';
import editanswerreducer from './editAnswerReducer';
import fetchphotoreducer from './getPhotoReducer';

//combines all the reducers as one reducer
const rootReducer = combineReducers({
    LoginReducer,
    profilereducer,
    searchreducer,
    ravereducer,
    postravereducer,
    drivereducer,
    homereducer,
    appraisalreducer,
    postappraisalreducer,
    searchnamereducer,
    updateprofilereducer,
    queryforumreducer,
    postquestionreducer,
    fetchAnswerByIdreducer,
    postanswerreducer,
    deletequestionreducer,
    deleteanswerreducer,
    editanswerreducer,
    fetchphotoreducer
});

export default rootReducer;