import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    result: [],
    statuscode: 0,
    isLoaded: false
}

//fetches answer based on question id
export default function fetchAnswerByIdreducer(state = initialState, action) {

    switch (action.type) {

        case allActions.GET_ALL_ANSWERS:
            debugger;
            return action;

        case allActions.RECEIVED_ANSWERS:
            debugger;
            return {
                ...state,
                isLoaded: true,
                result: action.payload.result,
                statuscode: action.payload.statuscode,
            }
        case allActions.FETCH_ANSWERS_ERROR:
            return {
                ...state,
                isLoaded: false,
                statuscode: action.payload.statuscode
            }
        default:
            return state;
    }
}