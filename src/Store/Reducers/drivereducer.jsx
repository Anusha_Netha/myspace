import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    result: [],
    statuscode: 0,
    isLoaded: false
}

//gets all drives from the database
export default function drivereducer(state = initialState, action) {

    switch (action.type) {
        case allActions.DRIVE_EXPORT:
            console.log("divereducerexported");

            return action;

        case allActions.RECEIVE_DRIVE_ALL:
            console.log("divereducer receieved");

            return {
                ...state,
                isLoaded: true,
                result: action.payload.drives,
                statuscode: action.payload.statuscode,
            }
        case allActions.DRIVE_DATA_ERROR:
            console.log("drive error");

            return {
                ...state,
                isLoaded: true,
                statuscode: action.payload.statuscode,
            }

        default:
            return state;
    }
}