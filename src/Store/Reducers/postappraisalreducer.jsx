import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    data: [],
    statuscode: '',
    isLoaded: false
}

//posts an appraisal request
export default function postappraisalreducer(state = initialState, action) {

    switch (action.type) {

        case allActions.POSTAPPRAISAL:
            return action;

        case allActions.POSTAPPRAISALSUCCESS:

            return {
                ...state,
                isLoaded: true,
                data: action.payload.appraisal,
                statuscode: action.payload.statuscode
            }
        case allActions.POST_APPRAISAL_DATA_ERROR:
            return {
                ...state,
                isLoaded: false,
                statuscode: action.payload.statuscode
            }
        default:
            return state;
    }
}