import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    details: {},
    statuscode: 0,
    isLoaded: false
}

//searches an employee based on their id
export default function searchreducer(state = initialState, action) {

    switch (action.type) {

        case allActions.SEARCH_EMPLOYEE_BY_ID:
            return action;

        case allActions.RECEIEVE_EMPLOYEE_DETAILS:
            debugger;
            return {
                ...state,
                isLoaded: true,
                details: action.payload.result,
                statuscode: action.payload.statuscode,
            }
        case allActions.SEARCH_NOT_EXISTS:
            debugger;
            return {
                ...state,
                statuscode: action.payload,
            }

        default:
            return state;
    }
}