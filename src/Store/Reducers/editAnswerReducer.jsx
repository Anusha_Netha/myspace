import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    statuscode: 0,
    isLoaded: false
}

//edits an answer 
export default function editanswerreducer(state = initialState, action) {

    switch (action.type) {

        case allActions.EDIT_ANSWER:
            return action;

        case allActions.EDIT_ANSWER_SUCCESS:

            return {
                ...state,
                isLoaded: true,
                statuscode: action.payload.statuscode
            }
        case allActions.EDIT_ANSWER_ERROR:
            return {
                ...state,
                isLoaded: false,
                statuscode: action.payload.statuscode
            }
        default:
            return state;
    }
}