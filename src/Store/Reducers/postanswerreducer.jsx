import * as allActions from '../Actions/actionConstant';

//intialises variables
const initialState = {
    statuscode: 0,
    isLoaded: false
}

//posts an answer
export default function postanswerreducer(state = initialState, action) {

    switch (action.type) {

        case allActions.POSTANSWER:
            return action;

        case allActions.POSTANSWERSUCCESS:

            return {
                ...state,
                isLoaded: true,
                statuscode: action.payload.statuscode
            }
        case allActions.POST_ANSWER_DATA_ERROR:
            return {
                ...state,
                isLoaded: false,
                statuscode: action.payload.statuscode
            }
        default:
            return state;
    }
}