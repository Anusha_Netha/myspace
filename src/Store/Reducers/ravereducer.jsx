import * as allActions from '../Actions/actionConstant';

//initialises variables
const initialState = {
    details: [],
    statuscode: '',
    isLoaded: false
}

//gets all the raves from the database
export default function ravereducer(state = initialState, action) {

    switch (action.type) {

        case allActions.RAVE_ALL:
            debugger;
            return action;

        case allActions.RECEIVE_RAVE_ALL:
            debugger;
            return {
                ...state,
                isLoaded: true,
                details: action.payload.result,
                statuscode: action.payload.statuscode,
            }

        default:
            return state;
    }
}