import *  as allActions from'./actionConstant';
/**
 * 
 * @param {this function is used for sending information to reducer } data 
 */
export function searchemployeeId(data){
 
    console.log(data);
  
    return{type:allActions.SEARCH_EMPLOYEE_BY_ID,payload:data};

}
/***
 * This function is used for getting information based on id  
 */
export function searchemployeeDetails(data){
    console.log("login success");
    return{
        type:allActions.RECEIEVE_EMPLOYEE_DETAILS,
        payload:data
    };
    
}
/***
 * This function executes when ever employee doesnot exists
 */
export function searchnotexists(data){
    console.log("login success");
    return{
        type:allActions.SEARCH_NOT_EXISTS,
        payload:data
    };
    
}