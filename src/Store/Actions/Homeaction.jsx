import *  as allActions from'./actionConstant';

/**
 * action for getting notification
 */
export function fetchNotification(){
debugger;
    return{type:allActions.NOTIFICATION};

}

/**
 * 
 * @param {action for receiving notification} data 
 */
export function receiveNotification(data){
debugger;
    return{
        type:allActions.RECEIVE_NOTIFICATION,
        payload:data
    };
    
}