import *  as allActions from'./actionConstant';
/**
 * 
 * @param {this function is used for searching employee by name} data 
 */
export function searchemployeeName(data){
 
    console.log(data);
  
    return{type:allActions.SEARCH_EMPLOYEE_BY_NAME,payload:data};

}

/**
 * This function  receives data if employee found 
 * @param {*} data 
 */
export function searchemployeeDetailsbyName(data){
    console.log("login success");
    return{
        type:allActions.RECEIEVE_EMPLOYEE_BY_NAME,
        payload:data
    };
    
}

/**
 * this function is used when employee is not found
 * @param {*} data 
 */
export function searchnotexists(data){
    console.log("login success");
    return{
        type:allActions.SEARCH_NOT_EXISTS,
        payload:data
    };
    
}