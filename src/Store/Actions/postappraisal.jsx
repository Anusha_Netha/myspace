import *  as allActions from'./actionConstant';

/**
 * 
 * @param {function for posting a Appraisal} data 
 */
export function postAppraisal(data){
     return{type:allActions.POSTAPPRAISAL,payload:data};
}
/**
 * 
 * @param {receiving status of appraisal} data 
 */
export function ReceiveStatusofAppraisal(data){
    return{
        type:allActions.POSTAPPRAISALSUCCESS,
        payload:data
    };
    
}