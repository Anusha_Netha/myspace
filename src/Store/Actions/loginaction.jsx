import *  as allActions from'./actionConstant';
/**
 * LoginUser Function for  sending the data
 * @param { } data 
 */
export function doLoginUser(data){
    console.log("userlogined");
    return{type:allActions.DO_LOGIN_USER,payload:data};

}
/**
 * receives data if login success
 * @param {*} data 
 */

export function loginUserSuccess(data){
    console.log("login success");
    return{type:allActions.LOGIN_USER_SUCCESS,payload:data};
    
}