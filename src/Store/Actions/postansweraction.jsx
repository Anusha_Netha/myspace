import *  as allActions from'./actionConstant';

/**
 * 
 * @param {function for posting a answer} data 
 */
export function postanswer(data){
     return{type:allActions.POSTANSWER,payload:data};
}
/**
 * 
 * @param {receiving status on id} data 
 */
export function ReceiveStatusofanswer(data){
    return{
        type:allActions.POSTANSWERSUCCESS,
        payload:data
    };
    
}