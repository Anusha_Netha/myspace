import *  as allActions from'./actionConstant';

/**
 * 
 * @param {function for posting a question} data 
 */
export function postQuestion(data){
     return{type:allActions.POST_QUESTION,payload:data};
}
/**
 * 
 * @param {action for posting question status} data 
 */
export function ReceivedStatus(data){
    return{
        type:allActions.POST_QUESTION_SUCCESS,
        payload:data
    };
    
}