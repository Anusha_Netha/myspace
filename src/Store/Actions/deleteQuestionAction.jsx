import *  as allActions from'./actionConstant';

/**
 * action for deleting a question
 */
export function deleteQuestion(data){
console.log('delete question')
debugger;
    return{type:allActions.DELETE_QUESTION,payload:data};

}

/**
 * 
 * @param {action for deleting a question} data 
 */
export function deleteQuestionSuccess(data){
console.log("delete question success");
debugger;
    return{
        type:allActions.DELETE_QUESTION_SUCCESS,
        payload:data
    };
    
}