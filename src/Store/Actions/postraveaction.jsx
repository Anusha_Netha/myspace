import *  as allActions from'./actionConstant';

/**
 * 
 * @param {function for posting a rave} data 
 */
export function postRave(data){
     return{type:allActions.POSTRAVE,payload:data};
}
/**
 * 
 * @param {function receiving status on posting rave} data 
 */
export function ReceiveStatusofRave(data){
    return{
        type:allActions.POSTRAVESUCCESS,
        payload:data
    };
    
}