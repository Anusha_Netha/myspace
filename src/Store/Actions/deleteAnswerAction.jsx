import *  as allActions from'./actionConstant';

/**
 * action for deleting a answer
 */
export function deleteAnswer(data){
console.log('delete answer')
debugger;
    return{type:allActions.DELETE_ANSWER,payload:data};

}

/**
 * 
 * @param {action for deleting a answer} data 
 */
export function deleteAnswerSuccess(data){
console.log("delete answer success");
debugger;
    return{
        type:allActions.DELETE_ANSWER_SUCCESS,
        payload:data
    };
    
}