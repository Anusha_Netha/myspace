import *  as allActions from'./actionConstant';
/**
 * action for fetch answer based on id 
 * @param {questionid} data 
 */
export function fetchAnswerById(data){
    debugger;
    return{type:allActions.GET_ALL_ANSWERS,payload:data};

}
/***
 * action for receiving answers based on id 
 */
export function receiveAnswerById(data){
    debugger;
    return{
        type:allActions.RECEIVED_ANSWERS,
        payload:data
    };
    
}