import *  as allActions from'./actionConstant';
/**
 * this function is used to update employee details based on id
 */
export function updateEmployeedetails(data){  
    return{type:allActions.UPDATE_VALUES,payload:data};

}

/**
 * this function is used when details of an employee is successfully updated
 * @param {*} data 
 */
export function updatedEmployeedetails(data){
    console.log(data);
    return{
        type:allActions.UPDATED_SUCCESSFULL,
        payload:data
    };
    
}