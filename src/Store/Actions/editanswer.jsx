import *  as allActions from'./actionConstant';
/***
 * action for edit answer 
 */
export function editAnswer(data){
    debugger;
    return{type:allActions.EDIT_ANSWER,payload:data};

}
/**
 * action for successful answer 
 */
export function answerSuccess(data){
    debugger;
    return{
        type:allActions.EDIT_ANSWER_SUCCESS,
        payload:data
    };
    
}