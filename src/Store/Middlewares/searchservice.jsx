import request from 'superagent';
import * as allActions from '../Actions/actionConstant'

const SearchService = store => next => action => {
    next(action)
    console.log(action.payload)
    debugger

    //based on action type data hits the database
    switch (action.type) {

        //gets the information of an employee based on id
        case allActions.SEARCH_EMPLOYEE_BY_ID:
            const id = localStorage.getItem('id')
            request.get('http://192.168.150.95:8080/myspace/searchemployee/' + `${id}` + `/${action.payload}`)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .then(res => {

                    const data = JSON.parse(res.text);
                    console.log(data);


                    //if success
                    if (data.statuscode == 200) {
                        console.log("data is display")
                        next({ type: allActions.RECEIEVE_EMPLOYEE_DETAILS, payload: data })
                    }

                    //error
                    else {
                        console.log("error")
                        next({
                            type: allActions.SEARCH_NOT_EXISTS,
                            payload: data.statuscode
                        })
                    }
                }
                )
                .catch(err => {
                    console.log("defaulterror");
                    return next({ type: 'SEARCH_NOT_EXISTS', err })
                });
            break;
        default:
            break;
    };
};
export default SearchService;