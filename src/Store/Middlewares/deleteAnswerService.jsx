import request from 'superagent';
import * as allActions from '../Actions/actionConstant'

const DeleteAnswerService = store => next => action => {
    next(action)
    console.log("delete question service");
    debugger;
    //based on action type it will get the data from database
    switch (action.type) {

        case allActions.DELETE_ANSWER:

            //if user wants to delete the answer then it will hit this api to delete that answer
            console.log(action.type);
            debugger;
            const id = localStorage.getItem('id')
            const payload = action.payload;
            request.delete('http://192.168.150.95:8080//myspace/queryforum/deleteAnswer/' + `${id}/` + `${payload}`)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .set('Accept', 'application/json')
                .then(res => {
                    debugger;
                    const data = JSON.parse(res.text);
                    console.log(data);

                    //if api is successfully executed
                    if (data.statuscode == 200) {
                        debugger;
                        console.log(data);

                        next({ type: allActions.DELETE_ANSWER_SUCCESS, payload: data })

                    }
                    //if api not executed properly
                    else {
                        console.log("error in delete question")
                        next({
                            type: allActions.DELETE_ANSWER_ERROR,
                            payload: data
                        })
                    }
                }
                )
                .catch(err => {
                    console.log("catch in delete question")
                    //  return next ({type:'DELETE_ANSWER_ERROR',err})
                });
            break;
        default:
            break;
    };
};
export default DeleteAnswerService;