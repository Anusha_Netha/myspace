import request from 'superagent';
import * as allActions from '../Actions/actionConstant'

const ProfileService = store => next => action => {
    next(action)

    console.log(action.payload)

    //hits the data based on action type
    switch (action.type) {

        //gets the data based employee id
        case allActions.FETCH_PROFILE_BY_ID:
            const id = localStorage.getItem('id');
            request.get('http://192.168.150.95:8080/myspace/profileview/' + `${id}`)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .then(res => {

                    const data = JSON.parse(res.text);
                    console.log(data);


                    //if success
                    if (data.statuscode == 200) {
                        console.log("data is display")
                        next({ type: allActions.RECEIEVE_PROFILE_DETAILS_BY_ID, payload: data })
                    }
                    else{
                        console.log("error")
                        next({
                        type: allActions.PROFILE_ERROR,
                        payload:data.statuscode})
                    }
                }
                )
                .catch(err => {
                    console.log("defaulterror");
                    return next({ type: 'PROFILE_ERROR', err })
                });
            break;
        default:
            break;
    };
};
export default ProfileService;