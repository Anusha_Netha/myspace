import request from 'superagent';
import * as allActions from '../Actions/actionConstant'

const DeleteQuestionService = store => next => action => {
    next(action)
    console.log("delete question service");
    debugger;

    //based on action type data gets hit into the database
    switch (action.type) {

        //if user wants to delete a question
        case allActions.DELETE_QUESTION:

            console.log(action.type);
            debugger;
            const id = localStorage.getItem('id')
            const payload = action.payload;
            request.delete('http://192.168.150.95:8080//myspace/queryforum/deletequestion/' + `${id}/` + `${payload}`)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .set('Accept', 'application/json')
                .then(res => {
                    debugger;
                    const data = JSON.parse(res.text);
                    console.log(data);
                    //data executed
                    if (data.statuscode == 200) {
                        debugger;
                        console.log(data);

                        next({ type: allActions.DELETE_QUESTION_SUCCESS, payload: data })

                    }
                    //if error occurs
                    else {
                        console.log("error in delete question")
                        next({
                            type: allActions.DELETE_QUESTION_ERROR,
                            payload: data
                        })
                    }
                }
                )
                .catch(err => {
                    console.log("catch in delete question")
                    //  return next ({type:'DELETE_QUESTION_ERROR',err})
                });
            break;
        default:
            break;
    };
};
export default DeleteQuestionService;