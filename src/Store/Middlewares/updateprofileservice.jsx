import request from 'superagent';
import * as allActions from '../Actions/actionConstant'

const UpdateProfileService = store => next => action => {
    next(action)
    console.log("Service login");
    debugger;

    //based on action type data hits the database
    switch (action.type) {

        //updates the values based on employee id
        case allActions.UPDATE_VALUES:
            console.log(action.type);
            debugger;
            request.put('http://192.168.150.95:8080/myspace/updateprofile', action.payload)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .set('Accept', 'application/json')
                .then(res => {
                    debugger;
                    const data = JSON.parse(res.text);
                    console.log(data);

                    //if success
                    if (data.status == 200) {
                        debugger;
                        console.log(data);

                        next({ type: allActions.UPDATED_SUCCESSFULL, payload: data })

                    }
                    //error
                    else {
                        console.log("error in service")
                        next({
                            type: allActions.UPDATE_ERROR,
                            payload: data
                        })
                    }
                }
                )
                .catch(err => {
                    console.log("catch in service")
                    return next({ type: 'UPDATE_ERROR', err })
                });
            break;
        default:
            break;
    };
};
export default UpdateProfileService;