import request from 'superagent';
import * as allActions from '../Actions/actionConstant'

const HomeService = store => next => action => {
    next(action)

    console.log(action.payload)

    //based on action type data hits the api to get the information
    switch (action.type) {

        //gets all the notification 
        case allActions.NOTIFICATION:
            const id = localStorage.getItem('id');
            request.get('http://192.168.150.95:8080/myspace/latestnotification/' + `${id}`)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .then(res => {

                    const data = JSON.parse(res.text);
                    console.log(data);


                    //if success
                    if (data.status == 200) {
                        console.log("data is display")
                        next({ type: allActions.RECEIVE_NOTIFICATION, payload: data })
                    }
                    // else{
                    //     console.log("error")
                    //     next({
                    //     type: allActions.NOTIFICATION_ERROR,
                    //     payload:data.statuscode})
                    // }
                }
                )
                .catch(err => {
                    console.log("defaulterror");
                    return next({ type: 'NOTIFICATION_ERROR', err })
                });
            break;
        default:
            break;
    };
};
export default HomeService;