import request from 'superagent';
import * as allActions from '../Actions/actionConstant'

const FetchAnswersByIdService = store => next => action => {
    next(action)
    debugger;
    console.log(action.payload)

    //based on action type data hits the api to get information
    switch (action.type) {

        //when a user wants to get all the answers from database
        case allActions.GET_ALL_ANSWERS:
            debugger;
            const id = localStorage.getItem('id');
            const data = action.payload;
            request.get(`http://192.168.150.95:8080/myspace/queryforum/getallanswersforquestion/` + `${id}` + `/${data}`)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .then(res => {

                    const data = JSON.parse(res.text);
                    console.log(data);

                    //if success
                    if (data.statuscode == 200) {
                        console.log("data is display")
                        next({ type: allActions.RECEIVED_ANSWERS, payload: data })
                    }
                    //error
                    else {
                        console.log("error")
                        next({
                            type: allActions.FETCH_ANSWERS_ERROR,
                            payload: data
                        })
                    }
                }
                )
                .catch(err => {
                    console.log("defaulterror");
                    return next({ type: 'FETCH_ANSWERS_ERROR', err })
                });
            break;
        default:
            break;
    };
};
export default FetchAnswersByIdService;