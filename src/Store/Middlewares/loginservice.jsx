import request from 'superagent';
import * as allActions from '../Actions/actionConstant'

const LoginService = store => next => action => {
    next(action)

    console.log("Service login");

    //based on action type data hits the api to get the information from database
    switch (action.type) {

        //when a user wants to login into application
        case allActions.DO_LOGIN_USER:
            debugger
            request.post('http://192.168.150.95:8080/myspace/login')
                .send({ username: action.payload.email, password: action.payload.password })
                .set('Content-Type', 'application/json')
                .then(res => {
                    const data = JSON.parse(res.text);


                    //if success
                    if (data.statuscode == 200) {
                        debugger;
                        localStorage.setItem('id', data.empId);
                        localStorage.setItem('jwt-token', data.token);
                        localStorage.setItem('auth-time',Date.now());

                        next({ type: allActions.LOGIN_USER_SUCCESS, payload: data.statuscode })

                    }
                    //error
                    else {
                        debugger
                        console.log("error in service")
                        next({
                            type: allActions.LOGIN_USER_DATA_ERROR,
                            payload: data.statuscode
                        })
                    }
                }
                )
                .catch(err => {
                    console.log("catch in service")
                    return next({ type: 'LOGIN_USER_DATA_ERROR', err })
                });
            break;
        default:
            break;
    };
};
export default LoginService;