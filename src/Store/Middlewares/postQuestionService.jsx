import request from 'superagent';
import * as allActions from '../Actions/actionConstant'

const PostQuestionService = store => next => action => {
    next(action)
    console.log("post question service");
    debugger;

    //hits the database based on the action type
    switch (action.type) {

        //posts a question into the database
        case allActions.POST_QUESTION:
            console.log(action.type);
            debugger;
            const id = localStorage.getItem('id')
            request.post('http://192.168.150.95:8080//myspace/queryforum/postquestion/' + `${id}`, action.payload)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .set('Accept', 'application/json')
                .then(res => {
                    debugger;
                    const data = JSON.parse(res.text);
                    console.log(data);

                    //if success
                    if (data.statuscode == 200) {
                        debugger;
                        console.log(data);

                        next({ type: allActions.POST_QUESTION_SUCCESS, payload: data })

                    }
                    //error
                    else {
                        console.log("error in service")
                        next({
                            type: allActions.POST_QUESTION_ERROR,
                            payload: data
                        })
                    }
                }
                )
                .catch(err => {
                    console.log("catch in service")
                    return next({ type: 'POST_QUESTION_ERROR', err })
                });
            break;
        default:
            break;
    };
};
export default PostQuestionService;