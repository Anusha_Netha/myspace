import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as postquestionaction from '../Store/Actions/postQuestion';

class SideNavQuery extends Component {

    constructor(props) {
        super(props);
        this.state = {
            //intialises variables
            fields:{},
            errors:{},
            isSubmitted:false,
        }
        //binds data
        this.handlePost = this.handlePost.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    componentWillMount(){
        const token = localStorage.getItem('jwt-token');
        const old_auth_time = localStorage.getItem('auth-time');
        const new_auth_time = Date.now();
        const session = new_auth_time - old_auth_time;

        if (session > 360000) {
            localStorage.removeItem('jwt-token')
            window.location.href = '/'
        }
    }

    //sets data if any input field is changes
    handleChange(field,e){
        let fields = this.state.fields;
        fields[e.target.name] = e.target.value;
        this.setState({fields});
    }

    //posts the question
    handlePost(e){
        e.preventDefault();
      if(this.validateQuestion()){
          console.log(this.state.fields['question'])
         this.props.postquestionaction.postQuestion({
            postedBy:localStorage.getItem('id'),
            question:this.state.fields['question']
         })
         this.setState({
             isSubmitted:true
         })
      }
      else{
          alert('Please Enter Valid Details')
      }
    }

    //validates question
    validateQuestion(){
        let fields = this.state.fields;
        let errors={};
        let questionValid = true;

        if(!fields['question']){
            questionValid = false;
            errors['question'] = "please enter question";
        }
        else if((fields['question'].length < 3 || fields['question'].length > 1000)){
            questionValid = false;
            errors['question'] = 'enter question in between 3 to 1000 characters only'
        }
        if (typeof fields["question"] !== "undefined") {
            if (!fields["question"].match(/^[ A-Za-z0-9_@.!,?/#&+-]*$/)) {
                questionValid = false;
                errors["question"] = "Please Enter Valid Information";
            }
        }
        this.setState({
            errors:errors
        })
        return questionValid
    }

    //closes the modal
    handleClose(e){
        e.preventDefault();
        let option = window.confirm('Do you want to close?');
        console.log(option);
        if(option){
            window.location.reload();
        }
    }

    //displays the data
    render() {
        console.log(this.props.statuscode)
        if(this.props.statuscode == 200 && this.state.isSubmitted){
            alert('your question is submitted successfully');
            window.location.reload();
        }
        if(this.props.statuscode == 201 && this.state.isSubmitted){
            alert('You cannot post question as your not authorized')
            this.setState({
                isSubmitted:false
            })
        }
        if(this.props.statuscode == 202 && this.state.isSubmitted){
            alert('Your Post is not submitted')
            this.setState({
                isSubmitted:false
            })
        }
        return (
            <div className="SideNavQuery">
                <div className="text-left">
                    <Link to='/home' className="link"><div className="list"><button className="fas fa-greater-than fa-x greater"></button>Home</div></Link>
                    <div className="list" data-target="#question" data-toggle="modal"><button className="fas fa-greater-than fa-x greater"></button>Ask a Question?</div>

                    {/* modal for post a question */}
                    <div class="modal" id="question">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Post a Question</h4>
                                    <button type="button" class="close"
                                    onClick={this.handleClose}>&times;</button>
                                </div>
                                <div class="modal-body">
                                    <input type="text" className="form-control"
                                     name="question" placeholder="Enter a Question?"
                                     ref="question"  value={this.state.fields['question']}
                                     onChange={this.handleChange.bind(this,'question')}/>
                                     <div style={{color:'red'}}>{this.state.errors['question']}</div>
                                    <input type="submit" className="btn mt-2" value="Post" onClick={this.handlePost}/>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn" 
                                    onClick={this.handleClose}>Close</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    debugger;
    console.log(state.postquestionreducer.data);
    return {
        statuscode: state.postquestionreducer.data,
    };
}
function mapDispatchToProps(dispatch) {
    debugger;
    return {
        postquestionaction: bindActionCreators(postquestionaction, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(SideNavQuery);