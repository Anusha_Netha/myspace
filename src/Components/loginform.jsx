import React, { Component } from 'react';
import '../Assets/css/loginpage.css';
import { Redirect, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as loginaction from '../Store/Actions/loginaction';
// component for login page 
class Login extends Component {
  // intiliazing the variables
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isSubmitted: false,
      nameError: '',
      passwordError: '',
    };

    //this  function binds the data
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.doLogin = this.doLogin.bind(this);
  }


  //whenever an input is changed this function is called
  handleNameChange = e => {
    this.setState({ email: e.target.value }, () => {
      this.validateName();
    });
  }
  // whwnec\vwer the password change this function is called
  handlePasswordChange = e => {
    this.setState({ password: e.target.value }, () => {
      this.validatePassword();
    })
  }
  //Login function with validations
  doLogin = e => {
    let flag = true;
    console.log("login entered");
    if (this.state.email == '' && this.state.password == '') {
      alert("Enter the details ")
      flag = false
    }
    else if (this.state.password == '' && this.state.email != '') {
      console.log(this.state.email.length)
      if(this.state.email.length >= 3 && this.state.email.length <= 20){
        alert("Enter password")
        flag = false
      }
      else{
        alert("Enter Valid Username and length should in between 3 to 20")
        flag = false
      }
      
    }
    else if (this.state.password != '' && this.state.email == '') {
      alert("Enter username")
      flag = false
    }
    else {
      this.props.loginaction.doLoginUser({
        email: this.state.email,
        password: this.state.password
      })
      flag = true;

    }
    //sets data
    if (flag) {
      this.setState({
        isSubmitted: true
      })
    }
  }

  //this function will render when a user press enter button to login
  onKeyPresstoLogin(e){
    let flag = true;
    console.log("login entered");
    if(e.key === 'Enter'){
      if (this.state.email === '' && this.state.password === '') {
        alert("Enter the details ")
        flag = false
      }
      else if (this.state.password === '' && this.state.email !== '') {
        console.log(this.state.email.length)
        if(this.state.email.length >= 3 && this.state.email.length <= 20){
          alert("Enter password")
          flag = false
        }
        else{
          alert("Enter Valid Username and length should in between 3 to 20")
          flag = false
        }
        
      }
      else if (this.state.password !== '' && this.state.email === '') {
        alert("Enter username")
        flag = false
      }
      else {
        this.props.loginaction.doLoginUser({
          email: this.state.email,
          password: this.state.password
        })
        flag = true;
  
      }
      //sets data
      if (flag) {
        this.setState({
          isSubmitted: true
        })
      }
    }
  }
  //this function will render when a user press enter to go to next field
  onKeyPresstoNext(e){
    console.log('entered into key press to next')
    if(e.key === 'Enter'){
      this.refs.password.focus();
    }
  }
  // validating the usernameInput field
  validateName = () => {
    const { email } = this.state;
    this.setState({
      nameError:
        email.length >= 3 && email.length <= 20 ? null : 'Name must be in between 3 to 20',
    });
  }
    // validating the userPasswordInput field
  validatePassword = () => {
    const { password } = this.state;
    let passwordcheck = false;
    for (let i = 0; i < password.length; i++) {
      if (password.charAt(i) >= 48 || password.charAt(i) <= 57) {
        passwordcheck = true;
      }
    }
    this.setState({
      passwordError:
        password.length >= 5 && password.length <= 20 && password.includes('@') && passwordcheck ? null : 'password should be in between 5 to 20 and must include @ and digits from(0 to 9)'
    })
  }
  render() {
    //based on login data following actions happens
    if (this.state.isSubmitted) {

      if ((this.props.isLoggedIn === true)) {

        console.log("token established and the user is validated");
        localStorage.setItem('username', this.state.email);
        window.location.reload();
        
        return <Redirect to="/home " />
      }
      else {
        if (this.props.statuscode == 204) {

          alert("Username or Password is incorrect");
          this.setState({ isSubmitted: false });
        }
        else if (this.props.statuscode == 202) {
          alert("Password is incorrect");
          this.setState({ isSubmitted: false });
        }
        else if(this.props.statuscode === 203)
        { 
          alert("Your Account is blocked try after some time");
          this.setState({ isSubmitted: false });
        }
        // else{
        //   alert("Invalid Credentails");
        //   this.setState({isSubmitted:false});
        // }

      }
    }
    return (
      //Login Form
      <div>
        <div className="login-form">
          <form >
            <div className="avatar">
              <i class="fa fa-user fa-5x" aria-hidden="true"></i>
            </div>
            <h2 className="text-center">Member Login</h2>
            <div className="form-group">
              <input type="text" className={`form-control ${this.state.nameError ? 'is-invalid' : ' '}`}
                id="email"
                name="email" value={this.state.email}
                onChange={this.handleNameChange}
                onKeyPress={this.onKeyPresstoNext.bind(this)}
                placeholder="Username"
                required="required" />
              <div className="invalid-feedback" style={{ color: "white", fontSize: '16px' }}>{this.state.nameError}</div>
            </div>
            {console.log(this.state.nameError)}

            <div className="form-group">
              <input type="password"
                className={`form-control ${this.state.passwordError ? 'is-invalid' : ' '}`}
                name="password"
                ref="password"
                value={this.state.password}
                onChange={this.handlePasswordChange}
                onKeyPress={this.onKeyPresstoLogin.bind(this)}
                placeholder="Password"
                required="required" />
              <div className="invalid-feedback" style={{ color: 'white', fontSize: '16px' }}>{this.state.passwordError}</div>
            </div>
          </form>
          <div className="form-group">
            <input type="submit" className="btn btn-primary btn-lg btn-block" value="Signin" onClick={this.doLogin} />
          </div>
        </div>

      </div>
    )
  }
}

function mapStateToProps(state) {
  console.log(state);
  return {
    isLoggedIn: state.LoginReducer.is_logged_in,
    statuscode: state.LoginReducer.statusCode
  };
}
function mapDispatchToProps(dispatch) {
  return {
    loginaction: bindActionCreators(loginaction, dispatch),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);