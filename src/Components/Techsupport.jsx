import React, { Component } from 'react'
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import SideNav from '../Components/SideNavigation';
class Techsupport extends Component {
    render() {
        // this arrary variable to store technical languages list
        let array = [{ title: "React Introduction", content: "https://www.youtube.com/watch?v=bUTsVY6VUQA&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS" },
        { title: "JSX Introduction", content: "https://www.youtube.com/watch?v=yc8fg7gWbBA&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS&index=2" },
        { title: "React Class and Functional Component", content: "https://www.youtube.com/watch?v=uGgPINlKqBs&index=3&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS" },
        { title: "React State and Props", content: "https://www.youtube.com/watch?v=DJtI3Pogd88&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS&index=4" },
        { title: "React Event Binding(Two way binding)", content: "https://www.youtube.com/watch?v=BWl9bw_nbbs&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS&index=5" },
        { title: "React Components Communication", content: "https://www.youtube.com/watch?v=dyL99ACQfsM&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS&index=6" },
        { title: "React Lists and Keys", content: "https://www.youtube.com/watch?v=tJYBMSuOX3s&index=7&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS" },
        { title: "React Fragments", content: "https://www.youtube.com/watch?v=UMo9_W8lPbs&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS&index=8" },
        { title: "React Life Cycle Hooks", content: "https://www.youtube.com/watch?v=kVyrzn29QPk&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS&index=9" },
        { title: "React Pure Components", content: "https://www.youtube.com/watch?v=PXXjkq4A-OU&index=10&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS" },
        { title: "React Rrouters", content: "https://www.youtube.com/watch?v=XRfD8xIOroA&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS&index=11" },
        { title: "react ref and dom", content: "https://www.youtube.com/watch?v=tiytyGEodl0&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS&index=12" },
        { title: "react typechecking and proptypes", content: "https://www.youtube.com/watch?v=vqwAw9ByRUw&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS&index=13" },
        { title: "react+flow", content: "https://www.youtube.com/watch?v=L0nJTyHBTtE&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS&index=14" },
        { title: "react typescript", content: "https://www.youtube.com/watch?v=tS6czSZW2oo&index=15&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS" },
        { title: "react highorder components", content: "https://www.youtube.com/watch?v=A9_9gQIkfx4&index=16&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS" },
        { title: "redux introduction", content: "https://www.youtube.com/watch?v=7Erbf5NXQQw&index=17&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS" },
        { title: "redux simple example", content: "https://www.youtube.com/watch?v=wZVzeob4ywc&index=18&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS" },
        { title: "redux tutorial 3", content: "https://www.youtube.com/watch?v=Fq15pkckMqQ&index=19&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS" },
        { title: "redux tutorial 4", content: "https://www.youtube.com/watch?v=Lt4P9BKOPfI&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS&index=20" },
        { title: "redux tutorial 5", content: "https://www.youtube.com/watch?v=Spui2z-m93g&index=21&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS" },
        { title: "redux tutorial 6", content: "https://www.youtube.com/watch?v=grZ4BVcFmeA&index=22&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS" },
        { title: "redux tutorial 7(redux thunk)", content: "https://www.youtube.com/watch?v=Sqkm39rqmEg&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS&index=23" },
        { title: "redux tutorial 8(redux saga)", content: "https://www.youtube.com/watch?v=eUMbH6X_Adc&list=PL7pEw9n3GkoWkHCQsfBh77UP4nc8lvTbS&index=24" }];

        //iterates the array
        const reactjs = array.map(item =>
            <div>
                {/* {<ul className="list-group"><li className="list-group-item">{item.title}-</li></ul>} */}
                {<div><h6>{item.title}</h6><a href={item.content}>{item.content}</a><hr/></div>}
            </div>)
        return (
            //displays list of successfully projects
            <div className="bg">
                <div >
                    <div>
                        <Header />
                        <div className="row">
                            <div className="col-md-3">
                                <SideNav />
                            </div>
                            <div className="col-md-9">
                                <div className="container ">
                                    <div className="text-left">
                                        <h2>React JS Tutorial Videos:</h2>
                                        <ul>{reactjs}</ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Footer />
                </div>
            </div>
        );

    }
}
export default Techsupport;