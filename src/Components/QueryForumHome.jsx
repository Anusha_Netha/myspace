import React, { Component }          from 'react';
import Header                        from '../Components/Header';
import Footer                        from './Footer';
import '../Assets/css/QueryForumHome.css';
import $                             from 'jquery';
import SideNavQuery                  from './SideNavQuery';
import { connect }                   from 'react-redux';
import { bindActionCreators }        from 'redux';
import * as queryforumaction         from '../Store/Actions/queryforumaction';
import * as fetchanswersaction       from '../Store/Actions/fetchAnswersById';
import * as postansweraction         from '../Store/Actions/postansweraction';
import * as deletequestion           from '../Store/Actions/deleteQuestionAction';
import * as deleteAnswer             from '../Store/Actions/deleteAnswerAction';
import * as editAnswer               from '../Store/Actions/editanswer';

class QueryForumHome extends Component {
    constructor(props) {
        super(props);
        //intialises the values
        this.state = {
            fields: {},
            errors: {},
            length: 0,
            questionId: 0,
            noanswer: "loading...",
            isSubmitted: true,
            answers : '',
            editanswerlength:'',
            editCheck : '',
            toggle:''
        }
        //binds the data
        this.handlePost = this.handlePost.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleEditChange = this.handleEditChange.bind(this);
        this.handleAnswerPost = this.handleAnswerPost.bind(this);
    }

    //this function is used to get all the questions posted previously
    componentWillMount() {
        const token = localStorage.getItem('jwt-token');
        const old_auth_time = localStorage.getItem('auth-time');
        const new_auth_time = Date.now();
        const session = new_auth_time - old_auth_time;

        if (session > 360000) {
            localStorage.removeItem('jwt-token')
            window.location.href = '/'
        }
        console.log("componentmounted")
        this.props.queryforumaction.getallquestions();
        }

    //this function is used to change the values 
    handleChange(field, e) {
        let fields = this.state.fields;
        fields[e.target.name] = e.target.value;
        this.setState({ fields});
    }

    //this function is used for editting a answer to change a value
    handleEditChange(e){
        this.setState({
           answers : e.target.value
        },()=>{
            this.validateEdittedAnswerRegex()
        })
    }

    //this function is used to post the answer when a user clicks on submit 
    handlePost(e) {
        e.preventDefault();
        if (this.validateAnswer()) {
            if(window.confirm("Are you sure you want to submit the answer")){
                 // alert("ok");
            console.log(this.state.answers)
            const questionId = localStorage.getItem("questionId")
            const id = localStorage.getItem('id')
            this.props.postansweraction.postanswer({
                postedBy: id,
                answer: this.state.fields["answer"],
                questionId: questionId
            })
            }
           else{
               alert("Thank You")
           }
        }
        else {
            alert('Please Enter Valid Details')
        }
    }

    //this function is used to post the editted answer when a user clicks on post
    handleAnswerPost(e){
        e.preventDefault();
        const empid = localStorage.getItem('id')
        const postedById = localStorage.getItem('postedbyId')
        if(empid == postedById){
            if(this.state.editCheck === null){
                console.log(this.state.answers)
                const questionId = localStorage.getItem('question')
                const answerId = localStorage.getItem('answerId')
                const id = localStorage.getItem('id')
                this.props.editAnswer.editAnswer({
                    postedBy : id,
                    answer : this.state.answers,
                    questionId : questionId,
                    answerId: answerId
                })
            }
            else{
                alert("Enter valid details")
                window.location.reload()
            }
    
        }
        else{
            alert("you're not authorized to edit this answer")
        }
        
    }

    //this function is used to validate the editted answer
    validateEdittedAnswerRegex(){
        let {answers} = this.state;
        this.setState({
            editCheck:
            ((/^[ A-Za-z0-9_@.!,/#&+-]*$/).test(answers)&&answers.length <= 65000)  ? null:'please enter valid information and length should in between 0 to 65000.',
        })
    }

    //this funcion is used to validate the answer
    validateAnswer() {
        let fields = this.state.fields;
        let errors = {};
        let answerValid = true;

        if (!fields['answer']) {
            answerValid = false;
            errors['answer'] = "please enter answer";
        }
        else if ((fields['answer'].length < 3 || fields['answer'].length > 65000)) {
            answerValid = false;
            errors['answer'] = 'enter answer in between 3 to 65000 characters only.'
        }
        if (typeof fields["answer"] !== "undefined") {
            if (!fields["answer"].match(/^[ A-Za-z0-9_@.!,/#&+-]*$/)) {
                answerValid = false;
                errors["answer"] = "Please Enter Valid Information";
            }
        }
        this.setState({
            errors: errors
        })
        return answerValid
    }

    //this function is used to close the modal
    handleClose(e) {
        e.preventDefault();
        let option = window.confirm('Do you want to close?');
        console.log(option);
        if (option) {
            window.location.reload();
            // $("#myModal").modal('hide');
        }
    }

    //this function is used to displays the list of answers that have been posted to question
    _allAnswers(questionid, id,length) {
        console.log(questionid)
        console.log(length)
        // $(".all_answers").slideToggle();
        let index = questionid.toString();
        index = "#" + index;
        if(this.state.toggle!='' && this.state.toggle!=index)
        {
            $(this.state.toggle).slideToggle(); 
        }
        this.props.fetchanswersaction.fetchAnswerById(id);
        for (let i = 0; i < length; i++) {
            if (questionid == i) {
                $(index).slideToggle();
                break;
            }
        }
        this.setState({
          toggle:index
        })
    }

    //this function is used to post the answer
    _postAnswer(questionId) {
        console.log(questionId)
        console.log(this.state.fields['answers']);
        localStorage.setItem('questionId', questionId);
    }

    //this function is used to delete the question
    _delete(postedby, id) {
        console.log(postedby);
        console.log(id);
        const empid = localStorage.getItem('id');
        console.log("hello im " + empid);
        if (empid == postedby) {
            if (window.confirm('Are you sure you want to delete this question')) {

                this.props.deletequestion.deleteQuestion(id)
            }
            else {
                alert('thank you')
            }
        }
        else {
            alert("you are not authorized to delete");
        }
    }

    //this function is used to delete a answer
    _deleteAnswer(answerId, id) {
        const empid = localStorage.getItem('id');
        if (empid == id) {
            // alert("ok")
            if (window.confirm("Are you sure you want to delete answer")) {
                this.props.deleteAnswer.deleteAnswer(answerId);
            }
            else {
                alert('thank you');
            }
        }
        else {
            alert("You're not authorized to delete")
        }
    }

    //this function is used to edit a answer
    _editAnswer(answerId,id,answer,questionId){
        // alert("ok" +answerId +" " + id + " " + answer)
        localStorage.setItem('answerId',answerId)
        localStorage.setItem('question',questionId)
        localStorage.setItem('postedbyId',id)
        this.setState({
            answers : answer
        })
        
        
    }

    //displays the information
    render() {
        console.log(this.props.questions)
        if (this.props.statuscode == 204 && this.state.isSubmitted) {
            //    document.getElementById("no_answer").innerHTML = "no answer is found"
            this.setState({
                noanswer: "answer is not found",
                isSubmitted: false
            })
            alert('no answers found')
        }
        if (this.props.status == 200) {
            alert("Successfully posted..")
            window.location.reload()
        }
        if (this.props.deletestatus == 200) {
            alert("deleted successfully!!")
            window.location.reload();
        }
        if (this.props.deleteanswerstatus == 200) {
            alert('deleted Successfully!!');
            window.location.reload();
        }
        if(this.props.editanswerstatus == 200){
            alert('edited successfully')
            window.location.reload()
        }
        if(this.props.editanswerstatus == 202){
            alert("unsuccessfull edit")
        }
        if(this.props.editanswerstatus == 203){
            alert('you didnot modified answer')
        }
        const empid = localStorage.getItem('id');
        return (
            <div className="bg">
                <Header />
                <div className="row">
                    <div className="col-md-3">
                        <SideNavQuery />
                    </div>
                    <div className="col-md-9">
                        <div className="queryBody container">
                            {
                                (this.props.questions) ?
                                    <div>
                                        <p className="text-left">Number of Questions = {this.props.questions.length}</p>
                                        {this.props.questions.map((question, index) => (
                                            <div className="query text-left" key={question.questionId}>
                                                <h5>Posted By&nbsp;:&nbsp;{question.postedBy}</h5>
                                                <p>Question&nbsp;:&nbsp;{question.question}</p>
                                                <div className="btn-groups">
                                                    <input type="submit" value="Post an Answer" className="btn btn1" data-target="#myModal" data-toggle="modal" onClick={this._postAnswer.bind(this, question.questionId)} />
                                                    <input type="submit" value="View all Answers" onClick={this._allAnswers.bind(this, index, question.questionId,this.props.questions.length)} className="btn btn1" />
                                                    
                                                    {   
                                                    question.postedBy ==empid ? <input type="submit" value="Delete Question" className="btn btn1 delete" onClick={this._delete.bind(this, question.postedBy, question.questionId)} /> :<div></div>
                                                
                                                    }
                                                </div>
                                                <div className="disable all_answers" id={index.toString()}>
                                                    {console.log(this.props.result)}
                                                    {
                                                        (this.props.result) ?
                                                            (this.props.result.length >=1) ?
                                                                <div>
                                                                    {this.props.result.map(res => (
                                                                        <div>
                                                                            <hr />
                                                                            <p>{res.answer}</p>
                                                                            <p>postedBy by&nbsp;:&nbsp;{res.postedBy}</p>
                                                                            {
                                                                                res.postedBy == empid ?
                                                                                <div className="ans">
                                                                                <input type="submit" value="Edit"  className="btn btn1" data-target= "#editAnswer" data-toggle="modal" onClick={this._editAnswer.bind(this,res.answerId,res.postedBy,res.answer,res.questionId)}  />
                                                                                <input type="submit" value="Delete" className="btn btn1" onClick={this._deleteAnswer.bind(this, res.answerId, res.postedBy)} />
                                                                                </div>
                                                                                :<div></div>
                                                                            }
                                                                            
                                                                        </div>
                                                                    ))}
                                                                </div>
                                                                 : 
                                                                <div>
                                                                    </div>
                                                            : <div id="no_answer">{this.state.noanswer}</div>
                                                    }
                                                </div>
                                            </div>
                                        ))}

                                    </div>
                                    : <div>Loading...</div>
                            }

                            {/* this modal is used for posting an answer */}
                            <div class="modal" id="myModal">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Post an Answer</h5>
                                            <button type="button" class="close" onClick={this.handleClose}>&times;</button>
                                        </div>
                                        <div class="modal-body text-left">
                                            <input type="text" name="answer"
                                                ref="answer"
                                                className="form-control"
                                                placeholder="Enter Answer"
                                                value={this.state.fields['answer']}
                                                onChange={this.handleChange.bind(this, "answer")} />
                                            <div style={{ color: 'red' }}>{this.state.errors['answer']}</div>
                                            <button type="button" className="btn btn1 mt-2" onClick={this.handlePost}>Post</button>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn1" onClick={this.handleClose}>Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {/* this modal is used to display edit answer */}
                            <div class="modal" id="editAnswer">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Edit Answer</h5>
                                            <button type="button" class="close" onClick={this.handleClose}>&times;</button>
                                        </div>
                                        <div class="modal-body text-left">
                                        {console.log(this.state.answer)}
                                            <input type="text" name="answers"
                                                ref="answers"
                                                className="form-control"
                                                placeholder="Enter Answer"
                                                defaultValue={this.state.answers}
                                                onChange={this.handleEditChange} />
                                            <div style={{ color: 'red' }}>{this.state.editanswerlength}</div>
                                            <div style={{color:'red'}}>{this.state.editCheck}</div>
                                            <button type="button" className="btn btn1 mt-2" onClick={this.handleAnswerPost}>Edit</button>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn1" onClick={this.handleClose}>Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

function mapStateToProps(state) {
    debugger;
    console.log(state.fetchAnswerByIdreducer.result);
    console.log(state.queryforumreducer.statuscode)
    return {
        questions: state.queryforumreducer.questions,
        StatusCode: state.queryforumreducer.statuscode,
        result: state.fetchAnswerByIdreducer.result,
        statuscode: state.fetchAnswerByIdreducer.statuscode,
        status: state.postanswerreducer.statuscode,
        deletestatus: state.deletequestionreducer.statuscode,
        deleteanswerstatus: state.deleteanswerreducer.statuscode,
        editanswerstatus :state.editanswerreducer.statuscode

    };

}
function mapDispatchToProps(dispatch) {
    debugger;
    return {
        queryforumaction: bindActionCreators(queryforumaction, dispatch),
        fetchanswersaction: bindActionCreators(fetchanswersaction, dispatch),
        postansweraction: bindActionCreators(postansweraction, dispatch),
        deletequestion: bindActionCreators(deletequestion, dispatch),
        deleteAnswer: bindActionCreators(deleteAnswer, dispatch),
        editAnswer :bindActionCreators(editAnswer, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(QueryForumHome);