import React, { Component } from 'react';
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import SideNav from '../Components/SideNavigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as postappraisalaction from '../Store/Actions/postappraisal';
import '../Assets/css/appraisal.css'

class Postappraisal extends Component {

    //initialises data
    constructor(props) {
        super(props);
        this.state = {
            empid: localStorage.getItem('id'),
            statuscode: 0,
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    componentWillMount(){
        const token = localStorage.getItem('jwt-token');
        const old_auth_time = localStorage.getItem('auth-time');
        const new_auth_time = Date.now();
        const session = new_auth_time - old_auth_time;

        if (session > 360000) {
            localStorage.removeItem('jwt-token')
            window.location.href = '/'
        }
    }
    //posts the data 
    // this function whenever the user clicks the submit button it post the appraisal action
    handleSubmit(event) {
        console.log(this.state.empid);
        this.props.postappraisalaction.postAppraisal(
            {
                employeeId: this.state.empid,

            }
        )

    }
    render() {
        //based on result data is displayed
        if (this.props.statuscode == 200) {
            alert("your request is submitted successfully ")
        }
        if (this.props.statuscode == 202) {
            alert("your request is not submitted")
        }
        if (this.props.statuscode == 201) {
            alert("your request has already submitted")
        }

        return (

            //Form to post Appraisal
            <div className="bg">
                <Header />
                <div className="row">
                    <div className="col-md-3">
                        <SideNav />
                    </div>
                    <div className="col-md-9">
                        <div class="container text-left appraisal-form">
                            <h2 className="text-center heading">Appraisal Request</h2>
                            <form>
                                <div class="form-group">
                                    <div><p>EmployeeID : &nbsp;{this.state.empid}</p></div>
                                </div>
                                <input type="submit" value="Submit" className="form-control btn" onClick={this.handleSubmit} />
                            </form>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        data: state.postappraisalreducer.data,
        statuscode: state.postappraisalreducer.statuscode
    };
}
function mapDispatchToProps(dispatch) {
    return {
        postappraisalaction: bindActionCreators(postappraisalaction, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Postappraisal);