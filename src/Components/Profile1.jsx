import React, { Component } from 'react';
import '../Assets/css/loginpage.css';
// import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as profileaction from '../Store/Actions/profileactions';
import '../Assets/css/Profile.css';
import SideNav from './SideNavigation';
import Header from './Header';
import Footer from './Footer';
import * as updateprofileaction from '../Store/Actions/updateprofileaction';
import axios, { put } from 'axios';
import * as photoaction from '../Store/Actions/getPhotoaction';


//component for EmployeeInfo
class Profile extends Component {

    //initialises variables
    constructor(props) {
        super(props);
        this.state = {
            details: [],
            isLoaded: false,
            personalemail: '',
            businessemail: '',
            workplacenumber: '',
            primarynumber: '',
            alternativenumber: '',
            skypeid: '',
            isSubmitted: true,
            file: null,
            error_workplace_num: '',
            error_pemail: '',
            error_bemail: '',
            error_primary_num: '',
            error_alt_MobileNumber: '',
            skypeId_error: '',
            change: false,
            changeEmailnumber: false,

        };
        this.handlePersonalEmailChange = this.handlePersonalEmailChange.bind(this);
        this.handleBusinessEmailChange = this.handleBusinessEmailChange.bind(this);
        this.handleWorkplaceNumberChange = this.handleWorkplaceNumberChange.bind(this);
        this.handlePrimaryNumberChange = this.handlePrimaryNumberChange.bind(this);
        this.handleAlternativeNumberChange = this.handleAlternativeNumberChange.bind(this);
        this.handleSkpeidChange = this.handleSkpeidChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.fileUpload = this.fileUpload.bind(this);
    }
    // this function validates the fetchEmployeedetails   d
    componentWillMount() {
        const token = localStorage.getItem('jwt-token');
        const old_auth_time = localStorage.getItem('auth-time');
        const new_auth_time = Date.now();
        const session = new_auth_time - old_auth_time;

        if (session > 360000) {
            localStorage.removeItem('jwt-token')
            window.location.href = '/'
        }

        console.log("componentmounted")
        this.props.profileaction.fetchemployeeId();

    }
    // whenever the personalEmailid changes this function validates
    handlePersonalEmailChange(e) {
        this.setState({ personalemail: e.target.value }, () => {
            this.validatePersonalEmail();
        })
        this.setState(
            {
                changeEmailnumber: true
            }
        )
    }
    // whenever the BusinessEmailid changes this function validates
    handleBusinessEmailChange(e) {
        this.setState({ businessemail: e.target.value }, () => {
            this.validateBusinessEmail();
        })
        this.setState(
            {
                changeEmailnumber: true
            }
        )
    }
    // whenever the workplaceNumber changes this function validates
    handleWorkplaceNumberChange(e) {
        this.setState({ workplacenumber: e.target.value }, () => {
            this.validateWorkplaceNumber();
        })
        this.setState({
            change: true
        })
    }
    // whenever the primaryNumber changes this function validates
    handlePrimaryNumberChange(e) {
        this.setState({ primarynumber: e.target.value }, () => {
            this.validatePrimaryNumber();
        })
        this.setState({
            change: true
        })
    }
    // whenever the Alternativenumber changes this function validates
    handleAlternativeNumberChange(e) {
        this.setState({ alternativenumber: e.target.value }, () => {
            this.validateAlternateNumber();
        })
        this.setState({
            change: true
        })
    }
    // whenever the Skypeid changes this function validates
    handleSkpeidChange(e) {
        this.setState({ skypeid: e.target.value }, () => {
            this.validateSkypeId();
        })
        this.setState(
            {
                changeEmailnumber: true
            }
        )
    }
    //validating PersonalEmailinputfield
    validatePersonalEmail() {
        const { personalemail } = this.state;
        this.setState({
            error_pemail:
                personalemail.length > 6 && personalemail.length < 40 && personalemail.includes("@") && personalemail.includes(".") ? null : 'Invalid Email',
        });
    }
    //validating BusinessEmailinputfield
    validateBusinessEmail() {
        const { businessemail } = this.state;
        this.setState({
            error_bemail:
                businessemail.length > 6 && businessemail.length < 40 && businessemail.includes("@") && businessemail.includes(".") ? null : 'Invalid Email',
        })
    }
    //validating workplaceNumberinputfield
    validateWorkplaceNumber() {
        const { workplacenumber } = this.state;
        this.setState({
            error_workplace_num:
                (/^[6-9]{1}[0-9]{9}$/).test(workplacenumber) ? null : 'Invalid Mobile Number',
        });

    }

    //validates primary number field
    validatePrimaryNumber() {
        const { primarynumber } = this.state;
        this.setState({
            error_primary_num:
                (/^[6-9]{1}[0-9]{9}$/).test(primarynumber) ? null : 'Invalid Mobile Number',
        });

    }
    //validates alternate number
    validateAlternateNumber() {
        const { alternativenumber } = this.state;
        this.setState({
            error_alt_MobileNumber:
                (/^[6-9]{1}[0-9]{9}$/).test(alternativenumber) ? null : 'Invalid Mobile Number',
        });

    }
    //validates skype id
    validateSkypeId() {
        const { skypeid } = this.state;
        this.setState({
            skypeId_error:
                skypeid.length > 6 && skypeid.length < 40 && skypeid.includes("@") && skypeid.includes(".") ? null : 'Invalid Email',
        })
    }
    //this function is used to set values when a user wants to update fields
    componentWillReceiveProps(newprops) {

        console.log(newprops.details)
        if (newprops.details) {
            this.setState({
                details: newprops.details,
                personalemail: newprops.details.personalEmailId,
                businessemail: newprops.details.businessEmailId,
                workplacenumber: newprops.details.workPlacePhoneNumber,
                primarynumber: newprops.details.primaryMobileNumber,
                alternativenumber: newprops.details.alternativeMobileNumber,
                skypeid: newprops.details.skypeId,

            })
        }


    }

    //this function is used to submit the data
    handleSubmit(event) {

        if (this.state.error_pemail === null && this.state.error_bemail === null &&
            this.state.error_primary_num === null && this.state.error_workplace_num === null
            && this.state.error_alt_MobileNumber === null && this.state.skypeId_error === null) {
            this.props.updateprofileaction.updateEmployeedetails({
                personalEmailId: this.state.personalemail,
                businessEmailId: this.state.businessemail,
                workPlacePhoneNumber: this.state.workplacenumber,
                primaryMobileNumber: this.state.primarynumber,
                alternativeMobileNumber: this.state.alternativenumber,
                skypeId: this.state.skypeid,
                employeeId: localStorage.getItem('id')
            });

            this.setState({
                isSubmitted: true
            })
        }
        else if (this.state.error_pemail != null || this.state.error_bemail != null ||
            this.state.error_primary_num != null || this.state.error_workplace_num != null
            || this.state.error_alt_MobileNumber != null || this.state.skypeId_error != null) {
            let primaryNum = 0;
            let workplaceNum = 0;
            let alternativeNum = 0;
            let flag = false;
            let flag1 = false;
            let check1 = false;
            let check2 = false;
            let check3 = false;
            {
                for (let i = 0; i < this.state.primarynumber.length; i++) {
                    if (this.state.primarynumber.charAt(i) >= 48 || this.state.primarynumber.charAt(i) <= 57) {
                        primaryNum++;
                        flag1 = true;
                    }
                }
                check1 = (/^[6-9]{1}[0-9]{9}$/).test(this.state.primarynumber);
            }
            {
                for (let i = 0; i < this.state.workplacenumber.length; i++) {
                    if (this.state.workplacenumber.charAt(i) >= 48 || this.state.workplacenumber.charAt(i) <= 57) {
                        workplaceNum++;
                        flag1 = true;
                    }
                }
                check2 = (/^[6-9]{1}[0-9]{9}$/).test(this.state.workplacenumber);
            }
            for (let i = 0; i < this.state.alternativenumber.length; i++) {
                if (this.state.alternativenumber.charAt(i) >= 48 || this.state.alternativenumber.charAt(i) <= 57) {
                    alternativeNum++;
                    flag1 = true;
                }
                check3 = (/^[6-9]{1}[0-9]{9}$/).test(this.state.alternativenumber);
            }

            {
                var regex = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+.com$/;
                var businessEmail = regex.test(this.state.businessemail);
                var personalEmail = regex.test(this.state.personalemail);
                var skypeId = regex.test(this.state.skypeid);

                flag = true;
                console.log(flag1)
            }
            if (this.state.change == false) {
                flag1 = false;
            }
            if (this.state.changeEmailnumber == false) {
                flag = false;
            }

            if ((primaryNum == this.state.primarynumber.length) && (workplaceNum == this.state.workplacenumber.length) && (alternativeNum == this.state.alternativenumber.length) && businessEmail && personalEmail && check1 && check2 && check3 && skypeId) {
                this.props.updateprofileaction.updateEmployeedetails({
                    personalEmailId: this.state.personalemail,
                    businessEmailId: this.state.businessemail,
                    workPlacePhoneNumber: this.state.workplacenumber,
                    primaryMobileNumber: this.state.primarynumber,
                    alternativeMobileNumber: this.state.alternativenumber,
                    skypeId: this.state.skypeid,
                    employeeId: localStorage.getItem('id')
                });
                this.setState({
                    isSubmitted: true
                })
            }
            else {
                if (flag1 == false && flag == true) {
                    alert('please check your Business email or Personal email or Skype Id is wrong')
                }
                else if (flag && flag1) {
                    alert('enter valid email and phonenumber')
                }
                else {
                    alert('enter valid phoneNumber')
                }
            }
        }
        else {
            alert('enter valid credentials')
        }

    }

    //this function sets the value to file input 
    onChange(e) {
        console.log('onchange')
        this.setState({ file: e.target.files[0] })
    }

    //this function is used to update a photo
    onSubmit(e) {
        let photo = false
        console.log('on submit');
        e.preventDefault() // Stop form submit
        if (this.state.file === null) {
            alert('choose a file to upload')
        }
        else {
            if (window.confirm("Are you sure you want to upload the image")) {
                debugger;
                this.fileUpload(this.state.file).then((response) => {
                    console.log(response.data);
                    console.log('updated')
                    debugger;
                    //   alert('updated successfully');
                    const data = response.data;

                    if (data.statuscode == 200) {
                        window.location.reload();
                        // this.props.photoaction.fetchPhoto(data.details.fileDownloadUri);
                        alert("ok")
                        photo = true;
                    }
                    if (data.statuscode == 204) {
                        alert('Not Updated')
                    }
                })

            }
            else {
                alert("Thank You")
            }

        }
    }

    //this function uploads a photo into database using axios
    fileUpload(file) {
        debugger;
        console.log("file upload")
        const id = localStorage.getItem('id');
        const url = 'http://192.168.150.95:8080/myspace/uploadFile/' + `${id}`;
        const formData = new FormData();
        formData.append('file', file)
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        console.log('return statement');
        return put(url, formData, config)
    }


    render() {

        if (this.props.statuscode === 200 && this.state.isSubmitted) {
            alert("updated successfully")
            window.location.reload();
            this.setState({
                isSubmitted: false
            })
        }
        else if (this.props.statuscode === 202 && this.state.isSubmitted) {
            alert('There is nothing to be Update')
            window.location.reload();
            this.setState({
                isSubmitted: false
            })
        }


        if (this.props.profilestatuscode === 200) {

            return (

                //displays employee details
                <div className="bg">

                    <Header />
                    <div className="row">
                        <div className="col-md-3">
                            <SideNav />
                        </div>
                        <div className="col-md-9">
                            <div className="container">

                                {
                                    (this.props.details) ?
                                        <div>
                                            <h3 className="mt-3">Employee Details</h3>
                                            <div className="row container">

                                                <div className="col-md-6">

                                                    <br />
                                                    <table className="photo text-left">
                                                        <tr>
                                                            <td>
                                                                <img src={`data:image/jpeg;base64,${this.props.photo}`} alt="emp_img" height="200px" width="200px" /><br />
                                                                <form onSubmit={this.onSubmit}><br />
                                                                    <input type="file" onChange={this.onChange} />
                                                                    <button type="submit">Upload</button>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <br />
                                                            <fieldset className="text-left">
                                                                <legend>Status</legend>

                                                                <div class="panel panel-default">
                                                                    <div class="panel-body">
                                                                        <p>Job Status:&nbsp; {this.props.details.jobStatus}</p>
                                                                        <p>Billable:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{this.props.details.billable}</p>
                                                                    </div>
                                                                </div>

                                                            </fieldset>
                                                        </tr>

                                                    </table>
                                                </div>
                                                <div className="col-md-6">
                                                    <br />
                                                    <table className="text-left">
                                                        <tbody>
                                                            <tr>
                                                                <td>Employee ID</td>
                                                                <td>{this.props.details.employeeId}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Name</td>
                                                                <td>{this.props.details.name}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>SezID</td>
                                                                <td>{this.props.details.sezId}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Sez Joining Date</td>
                                                                <td>{this.props.details.sezJoiningDate}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Employee Type</td>
                                                                <td>{this.props.details.employmentType}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Department</td>
                                                                <td>{this.props.details.department}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Designation</td>
                                                                <td>{this.props.details.designation}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Practice</td>
                                                                <td>{this.props.details.practice}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Competency</td>
                                                                <td>{this.props.details.competency}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Work Location</td>
                                                                <td>{this.props.details.workLocation}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Permanent Location</td>
                                                                <td>{this.props.details.permanentLocation}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Company EmailId</td>
                                                                <td>{this.props.details.companyEmailId}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Personal EmailId</td>
                                                                <td><input type="text"
                                                                    defaultValue={this.props.details.personalEmailId}
                                                                    size="40"
                                                                    name="personalemail"
                                                                    className={`form-control ${this.state.error_pemail ? 'is-invalid' : ''}`}
                                                                    onChange={this.handlePersonalEmailChange}
                                                                /><small id="headingHelp" className="form-text text-muted" />
                                                                    <div className='invalid-feedback'>{this.state.error_pemail}</div></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Business EmailId</td>
                                                                <td><input type="text"
                                                                    defaultValue={this.props.details.businessEmailId}
                                                                    size="40"
                                                                    name="businessemail"
                                                                    className={`form-control ${this.state.error_bemail ? 'is-invalid' : ''}`}
                                                                    onChange={this.handleBusinessEmailChange} />
                                                                    <small id="headingHelp" className="form-text text-muted" />
                                                                    <div className='invalid-feedback'>{this.state.error_bemail}</div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Workplace Number</td>
                                                                <td><input type="text"
                                                                    defaultValue={this.props.details.workPlacePhoneNumber}
                                                                    size="40"
                                                                    name="workplacenumber"
                                                                    className={`form-control ${this.state.error_workplace_num ? 'is-invalid' : ''}`}
                                                                    onChange={this.handleWorkplaceNumberChange} />
                                                                    <small id="headingHelp" className="form-text text-muted" />
                                                                    <div className='invalid-feedback'>{this.state.error_workplace_num}</div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Primary PhoneNumber</td>
                                                                <td><input type="text"
                                                                    defaultValue={this.props.details.primaryMobileNumber}
                                                                    size="40"
                                                                    name="primarynumber"
                                                                    className={`form-control ${this.state.error_primary_num ? 'is-invalid' : ''}`}
                                                                    onChange={this.handlePrimaryNumberChange} />
                                                                    <small id="headingHelp" className="form-text text-muted" />
                                                                    <div className='invalid-feedback'>{this.state.error_primary_num}</div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Alternative MobileNumber</td>
                                                                <td><input type="text"
                                                                    defaultValue={this.props.details.alternativeMobileNumber}
                                                                    size="40"
                                                                    name="alternativenumber"
                                                                    className={`form-control ${this.state.error_alt_MobileNumber ? 'is-invalid' : ''}`}
                                                                    onChange={this.handleAlternativeNumberChange} />
                                                                    <small id="headingHelp" className="form-text text-muted" />
                                                                    <div className='invalid-feedback'>{this.state.error_alt_MobileNumber}</div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Skype ID</td>
                                                                <td><input type="text"
                                                                    defaultValue={this.props.details.skypeId}
                                                                    size="40"
                                                                    className={`form-control ${this.state.skypeId_error ? 'is-invalid' : ''}`}
                                                                    name="skypeid"
                                                                    onChange={this.handleSkpeidChange} />
                                                                    <small id="headingHelp" className="form-text text-muted" />
                                                                    <div className='invalid-feedback'>{this.state.skypeId_error}</div>
                                                                </td>
                                                            </tr>
                                                            <tr><td colspan="2"><input type="submit" value="SAVE" onClick={this.handleSubmit} /></td></tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        :
                                        <div>Loading...</div>
                                }

                            </div>
                        </div>

                    </div>
                    <Footer />
                </div>
            );
        }
        else {
            return (
                <div className="bg">
                    <Header />
                    <div className="row">
                        <div className="col-md-3">
                            <SideNav />
                        </div>
                        <div className="col-md-9">
                            {/* <Body /> */}
                            Time out
                    </div>
                    </div>
                    <Footer />
                </div>
            )
        }
    }

}
function mapStateToProps(state) {
    console.log(state);
    return {
        details: state.profilereducer.details,
        isLoaded: state.profilereducer.isLoaded,
        profilestatuscode: state.profilereducer.statuscode,
        employee_details: state.updateprofilereducer.updated_employee_details,
        statuscode: state.updateprofilereducer.statuscode,
        photo: state.fetchphotoreducer.data,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        profileaction: bindActionCreators(profileaction, dispatch),
        updateprofileaction: bindActionCreators(updateprofileaction, dispatch),
        photoaction: bindActionCreators(photoaction, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Profile);