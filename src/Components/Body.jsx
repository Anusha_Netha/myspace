import React, { Component } from 'react';
import notification from '../Assets/Images/notification.png';
import '../Assets/css/Body.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as homeaction from '../Store/Actions/Homeaction';

// component for body
class Body extends Component {
    //initialises variables
    constructor(props) {
        super(props);
        this.state = {
            statuscode: 0,
            value: false,
            isSubmitted: false,


        }
    }
    // this function mounts the fetching notification
    componentWillMount() {
        const token = localStorage.getItem('jwt-token');
        const old_auth_time = localStorage.getItem('auth-time');
        const new_auth_time = Date.now();
        const session = new_auth_time - old_auth_time;

        if (session > 360000) {
            localStorage.removeItem('jwt-token')
            window.location.href = '/'
        }
        this.props.homeaction.fetchNotification();

    }
    render() {
        // displays the search byId and displays the notification
        return (

            <div>

                <div className="row">
                    <div className="col-md-12 col-sm-12 body text-left">
                        {
                            (this.props.notification) ?
                                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                                    <div class="mainflip">
                                        <div class="frontside">
                                            <div class="card">
                                                <div class="card-body text-center">
                                                    <p><img class=" img-fluid" src={notification} alt="card image" /></p>
                                                    <h4 class="card-title">Notification</h4>
                                                    {/* <p class="card-text">This is basic card with image on top, title, description and button.</p>
                                                        <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a> */}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="backside">
                                            <div class="card">
                                                <div class="card-body text-left mt-4">
                                                    <h4 class="card-title text-center">Notification</h4>
                                                    <p class="card-text">Name of Event : {this.props.notification.title}</p>
                                                    <p class="card-text">Date on&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:{this.props.notification.driveDate}</p>
                                                    <p class="card-text">Description&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:{this.props.notification.discription}</p>
                                                    <p class="card-text">CreatedBy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:{this.props.notification.createdBy}</p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                : <div>Loading...</div>

                        }

                    </div>


                </div>

            </div>
        );

    }
}

function mapStateToProps(state) {
    debugger;
    console.log(state);
    return {
        notification: state.homereducer.notification,
        StatusCode: state.homereducer.status,
    };

}
function mapDispatchToProps(dispatch) {
    debugger;
    return {
        homeaction: bindActionCreators(homeaction, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Body);