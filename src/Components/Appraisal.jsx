import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Footer from '../Components/Footer';
import Header from '../Components/Header';
import SideNav from '../Components/SideNavigation';
import * as appraisalaction from '../Store/Actions/fetchappraisalaction';
import '../Assets/css/Rave.css'
// component for AppraisalStatus


class Appraisal extends Component {
    // intiliazes the variables
    constructor(props) {
        super(props);
        this.state = {}
    }
    // this function willl be mounted to fetch the appraisal details 
    componentWillMount() {
        const token = localStorage.getItem('jwt-token');
        const old_auth_time = localStorage.getItem('auth-time');
        const new_auth_time = Date.now();
        const session = new_auth_time - old_auth_time;

        if (session > 360000) {
            localStorage.removeItem('jwt-token')
            window.location.href = '/'
        }
        console.log("componentmounted")
        this.props.appraisalaction.fetchAppraisal();

    }
    //this will displays information
    render() {
        return (
            //Displays all appraisals
            <div className="bg">
                <Header />
                <div className="row">
                    <div className="col-md-3 col-sm-3">
                        <SideNav />
                    </div>
                    <div className="col-md-9 col-sm-9">
                        <div className="container">
                            {
                                (this.props.details) ?

                                (this.props.details)?
                                    <div>
                                        <table className="table text-left">
                                            <tr><td><b>Employee ID</b></td><td>{this.props.details.employeeId}</td></tr>
                                            <tr><td><b>Performance Points</b></td><td>{this.props.details.performancePoints}</td></tr>
                                            <tr><td><b>Approved By</b></td><td>{this.props.details.approvedBy}</td></tr>
                                            <tr><td><b>Approval Status</b></td><td>{this.props.details.approvalStatus}</td></tr>
                                            <tr><td><b>Approval Date</b></td><td>{this.props.details.approvalDate}</td></tr>

                                        </table>
                                    </div>
                                    :<div>Loading...</div>
                                    :
                                    <div>Sorry,No Records Found!!</div>
                            }

                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}
function mapStateToProps(state) {
    debugger;
    console.log(state);
    return {
        details: state.appraisalreducer.details,
    };
}
function mapDispatchToProps(dispatch) {
    debugger;
    return {
        appraisalaction: bindActionCreators(appraisalaction, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Appraisal);