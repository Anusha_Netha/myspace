import React, { Component } from 'react'
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import SideNav from '../Components/SideNavigation';
import '../Assets/css/holidays.css';

// component for holidays Feature
class Holidays extends Component {
    render() {
        // displaying the tables for 
        return (
            <div className="bg">
                <Header />
                <div className="row">
                    <div className="col-md-3">
                        <SideNav />
                    </div>
                    <div className="col-md-9">
                        {/* <Body /> */}
                        <div class="row container">

                            <div class="col-md-6">
                                <p class="t1">Indiaholidays</p>
                                <p class="t12">Holidayslist</p>
                                <table class="table text-left holidaytable">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Month</th>
                                            <th>Date</th>
                                            <th>Day </th>
                                            <th>Occasion </th>
                                        </tr>
                                    </thead>
                                    <tbody className="tablecolumn">
                                        <tr>
                                            <td>January</td>
                                            <td>1-1-2019</td>
                                            <td>Tuesday</td>
                                            <td>NewYear</td>
                                        </tr>
                                        <tr>
                                            <td>January</td>
                                            <td>15-1-2019</td>
                                            <td>Tuesday</td>
                                            <td>MakaraSankranti</td>
                                        </tr>
                                        <tr>
                                            <td>March</td>
                                            <td>20-3-2019</td>
                                            <td>Wednesday</td>
                                            <td>Ugadi</td>
                                        </tr>
                                        <tr>
                                            <td>June</td>
                                            <td>05-06-2019</td>
                                            <td>Wednesday</td>
                                            <td>Ramzan</td>
                                        </tr>
                                        <tr>
                                            <td>August</td>
                                            <td>15-08-2019</td>
                                            <td>Thursday</td>
                                            <td>Independenceday</td>
                                        </tr>
                                        <tr>
                                            <td>September</td>
                                            <td>02-09-2019</td>
                                            <td>Monday</td>
                                            <td>GaneshChavithi</td>
                                        </tr>
                                        <tr>
                                            <td>October</td>
                                            <td>09-10-2019</td>
                                            <td>Tuesday</td>
                                            <td>VijayaDashami</td>
                                        </tr>
                                        <tr>
                                            <td>December</td>
                                            <td>25-12-2019</td>
                                            <td>Wednesday</td>
                                            <td>Christmas</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-6">
                                <p class="t2">USholidays</p>
                                <p class="t22">Holidayslist</p>
                                <table class="table text-left holidaytable">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Month</th>
                                            <th>Date</th>
                                            <th>Day </th>
                                            <th>Occasion </th>
                                        </tr>
                                    </thead>
                                    <tbody className="tablecolumn">
                                        <tr>
                                            <td>January</td>
                                            <td>1-1-2019</td>
                                            <td>Tuesday</td>
                                            <td>NewYear</td>
                                        </tr>
                                        <tr>
                                            <td>May</td>
                                            <td>15-1-2019</td>
                                            <td>Monday</td>
                                            <td>Memorialday</td>
                                        </tr>
                                        <tr>
                                            <td>July</td>
                                            <td>20-3-2019</td>
                                            <td>Thursday</td>
                                            <td>Independenceday</td>
                                        </tr>
                                        <tr>
                                            <td>September</td>
                                            <td>05-06-2019</td>
                                            <td>Monday</td>
                                            <td>Labor</td>
                                        </tr>
                                        <tr>
                                            <td>November</td>
                                            <td>15-08-2019</td>
                                            <td>Thursday</td>
                                            <td>Vertensday</td>
                                        </tr>
                                        <tr>
                                            <td>November</td>
                                            <td>02-09-2019</td>
                                            <td>Monday</td>
                                            <td>ThanksgivingDay</td>
                                        </tr>
                                        <tr>
                                            <td>November</td>
                                            <td>09-10-2019</td>
                                            <td>Tuesday</td>
                                            <td>Day after Thanks giving day</td>
                                        </tr>
                                        <tr>
                                            <td>December</td>
                                            <td>25-12-2019</td>
                                            <td>Wednesday</td>
                                            <td>Christmas</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
                <Footer />
            </div>

        );

    }
}
export default Holidays;