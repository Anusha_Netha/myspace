import React, { Component } from 'react';
import '../Assets/css/Footer.css';
import ribbon from '../Assets/Images/ribbon.jpeg';
//component class for footer
class Footer extends Component {

    //initialises variables
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        //displays the footer
        return ( 
            <div className="footer row">
                <div className="col-md-6 col-sm-12 text-left ">
                    <div className="info">
                        <h3><span className="inno">Inno</span><span className="minds">minds</span></h3>
                        <p className="copyrights">Copyright &copy;2019, <a href="https://www.innominds.com/">Innominds Software Inc.</a> All rights reserved.</p>
                        <p className="email"><a>myspace@innominds.com</a></p></div>
                </div>

                <div className="col-md-6 col-sm-12 text-right">
                    <img src={ribbon} alt="ribbon"  className="ribbon" />
                    <div className="textonimg">
                        <p>No.of user Logged in:100</p>
                    </div>
                </div>

            </div>
         );
    }
}
 
export default Footer;