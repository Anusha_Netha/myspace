import React, { Component } from 'react';
import '../Assets/css/SideNavigation.css';
import $ from 'jquery';
import { Link } from 'react-router-dom';

class SideNav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            opened: -1,

        }
    }

    //jquery function to toggle the nav bar
    _toggleDiv(id) {
        let value = this.state.opened;

        console.log();
        if (id == 1 || value == 1) {
            $(this.refs['empopen']).slideToggle();
        }
        if (id == 2 || value == 2) {
            $(this.refs['pefopen']).slideToggle();
        }
        if (id == 3 || value == 3) {
            $(this.refs['awardsopen']).slideToggle();
        }
        if (id == 4 || value == 4) {
            $(this.refs['libraryopen']).slideToggle();
        }
        if (id == 5 || value == 5) {
            $(this.refs['clientopen']).slideToggle();
        }
        if (id == 6 || value == 6) {
            $(this.refs['docopen']).slideToggle();
        }
        this.setState({ opened: id });
    }

    render() {

        return (
            //displays the nav bar vertically
            <div className="sidenav">
                <div className="text-left">
                    <Link to="/profile" className="link"><div className="list"><button className="fas fa-greater-than fa-x greater"></button>Employee Info</div></Link>
                    <div className="list" onClick={this._toggleDiv.bind(this, 2)}><button className="fas fa-greater-than fa-x greater"></button>Performance Appraisal</div>
                    <div ref="pefopen" className="disable">
                        <Link to="/appraisal" className="link"><li className="options opt">AppraisalStatus</li></Link>
                        <Link to="/postappraisal" className="link"><li className="options">Request for Appraisal</li></Link>
                    </div>
                    <div className="list" onClick={this._toggleDiv.bind(this, 3)}><button className="fas fa-greater-than fa-x greater"></button>Technical</div>
                    <div ref="awardsopen" className="disable">
                        <Link to="/techsupport" className="link"><li className="options opt">Tech Support</li></Link>
                        <Link to="/successfullproject" className="link"><li className="options">Successfull Project</li></Link>
                    </div>
                    <div className="list" onClick={this._toggleDiv.bind(this, 4)}><button className="fas fa-greater-than fa-x greater"></button>RAVE</div>
                    <div ref="libraryopen" className="disable">
                        <Link to="/raveall" className="link"><li className="options opt">RAVE </li></Link>
                        <Link to="/postrave" className="link"><li className="options">Post a RAVE </li></Link>
                    </div>
                    <Link to="/drives" className="link"><div className="list"><button className="fas fa-greater-than fa-x greater"></button>Drives</div></Link>
                    <Link to='/holidays' className="link"><div className="list"><button className="fas fa-greater-than fa-x greater"></button>Holidays</div></Link>
                    <Link to="/queryForum" className="link"><div className="list" onClick={this._toggleDiv.bind(this, 6)}><button className="fas fa-greater-than fa-x greater"></button>QueryForum</div></Link>
                </div>
            </div>
        );
    }
}

export default SideNav;