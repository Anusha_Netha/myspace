import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as  driveaction from '../Store/Actions/driveaction';
import Header from '../Components/Header';
import SideNav from '../Components/SideNavigation';
import Footer from '../Components/Footer';
import '../Assets/css/drive.css';

class Drive extends Component {

    //initialises the variables
    constructor(props) {
        super(props);
        this.state = {}
    }
    // this function will mount to fetch the drive
    componentWillMount() {
        const token = localStorage.getItem('jwt-token');
        const old_auth_time = localStorage.getItem('auth-time');
        const new_auth_time = Date.now();
        const session = new_auth_time - old_auth_time;

        if (session > 360000) {
            localStorage.removeItem('jwt-token')
            window.location.href = '/'
        }
        console.log("componentmounted")

        this.props.driveaction.fetchdrive();

    }

    render() {
        debugger;
        //displays data based on drive result
        console.log("data is rendering");
        console.log(this.props.result);
        return (
            <div>
                <div className="bg">
                    <Header />
                    <div className="row">
                        <div className="col-md-3 col-sm-3">
                            <SideNav />
                        </div>
                        <div class="col-lg-9 col-sm-9">
                            <div class="container mb-5 mt-5">
                                <div class="pricing card-deck flex-column flex-md-row mb-3">
                                    {
                                        (this.props.result) ?
                                            <div>
                                                {this.props.result.map(item => (
                                                    <div class="card card-pricing text-center px-3 mb-4">
                                                        <span class="h6 w-60 mx-auto px-4 py-1 rounded-bottom bg-primary text-white shadow-sm">{item.title}</span>

                                                        <div class="card-body pt-0">
                                                            <ul class="list-unstyled mb-4 text-left">
                                                                <li><b>Details</b> : {item.discription}</li>
                                                                <li>Date : {item.driveDate}</li>
                                                                <li>Posted By : {item.createdBy}</li>
                                                                <li>Posted On : {item.createdAt}</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                ))}
                                            </div>
                                            : <div>Loading..</div>
                                    }

                                </div>
                            </div>
                        </div>
                    </div>
                    <Footer />
                </div>


            </div>

        );
    }
}
function mapStateToProps(state) {
    debugger;
    console.log(state);
    return {
        result: state.drivereducer.result,
        statuscode: state.drivereducer.statuscode
    };
}
function mapDispatchToProps(dispatch) {
    debugger;
    return {
        driveaction: bindActionCreators(driveaction, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Drive);