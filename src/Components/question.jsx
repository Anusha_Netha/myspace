import React, { Component } from 'react';
import Header from './Header';
import SideNavQuery from './SideNavQuery';
import Footer from './Footer';

class Question extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields:{},
            errors:{},
        }
        this.handlePost = this.handlePost.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }
    handleChange(field,e){
        let fields = this.state.fields;
        fields[e.target.name] = e.target.value;
        this.setState({fields});
    }
    handlePost(e){
        e.preventDefault();
      if(this.validateAnswer()){
          
          alert("ok");
      }
      else{
          alert('Please Enter Valid Details')
      }
    }
    validateAnswer(){
        let fields = this.state.fields;
        let errors={};
        let questionValid = true;

        if(!fields['question']){
            questionValid = false;
            errors['question'] = "please enter question";
        }
        else if((fields['question'].length < 3 || fields['question'].length > 255)){
            questionValid = false;
            errors['question'] = 'enter question in between 3 to 255 characters only'
        }
        this.setState({
            errors:errors
        })
        return questionValid
    }
    handleClose(e){
        e.preventDefault();
        let option = window.confirm('Do you want to close?');
        console.log(option);
        if(option){
            window.location.reload();
        }
    }
    render() { 
        return ( 
            <div className="bg">
                <Header/>
                <div className="row">
                    <div className="col-md-3">
                        <SideNavQuery/>
                    </div>
                    <div className="col-md-9">
                        <h5>Post a Question</h5>
                        <form className="text-left">
                            <label htmlFor="question">Enter a Question</label>
                            <input type="text" name="question"
                            className="form-control"
                            onChange={this.handleChange.bind(this,'question')}
                            value={this.state.fields['question']}
                            ref="question" placeholder="Enter a question"/>
                            <div style={{color:'red'}}>{this.state.errors['question']}</div>
                            <input type="submit" className="btn mt-2" 
                            onClick={this.handlePost} value="Post" />
                            <input type="submit" className="btn mt-2"
                            onClick={this.handleClose} value="Cancel"/>
                        </form>
                    </div>
                </div>
                <Footer/>
            </div>
         );
    }
}
 
export default Question;