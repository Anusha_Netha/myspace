import React, { Component } from 'react';
import '../Assets/css/Header.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as profileaction from '../Store/Actions/profileactions';
import { Redirect, Link } from 'react-router-dom';
// component for header in Homepage
class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            details: {},
            isLoaded: false,
            valid: false,
            searchString: ' ',
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        // this.handleSubmit = this.handleSubmit.bind(this);
        this.handleKeySearch = this.handleKeySearch.bind(this);
    }
    //this function will mounted to fetch employeeId
    componentWillMount() {
        const token = localStorage.getItem('jwt-token');
        const old_auth_time = localStorage.getItem('auth-time');
        const new_auth_time = Date.now();
        const session = new_auth_time - old_auth_time;

        if (session > 360000) {
            localStorage.removeItem('jwt-token')
            window.location.href = '/'
        }
        console.log("componentmounted");
        this.props.profileaction.fetchemployeeId();
    }

    //logout function
    DoLogout = e => {
        localStorage.removeItem("jwt-token");
        localStorage.removeItem("id");
        localStorage.removeItem("username");
        localStorage.removeItem('answerId');
        localStorage.removeItem('auth-time');
        localStorage.removeItem('postedbyId');
        localStorage.removeItem('question');
        localStorage.removeItem('questionId')
        this.setState({ valid: true });
        window.location.reload();

    }

    //this function is used when an input is changed
    handleInputChange(e) {
        console.log(this.state.searchString)
        this.setState({
            searchString: e.target.value,
        });
        console.log(this.state.searchString);
    }

    //this function  is used to search an employee
    // handleSubmit(value) {
    //     console.log(this.state.searchString);
    //     console.log(value)
    //     if (this.state.searchString != " ") {
    //         window.location.href = "/search/" + value
    //     }
    // }

    //this function is used to search an employee when enter button is pressed
    handleKeySearch(e, value) {

        if (e.key == 'Enter') {
            if (this.state.searchString != " ") {
                window.location.href = "/search/" + value
            }
        }
    }
    render() {
        // if valid after it will redirect to its login Page
        if (this.state.valid) {
            return <Redirect to="/"></Redirect>
        }
        return (
            /**
             * jumbotron block to have a logo and employee details
             */
            <div>
                <div className="row jumbotron" id="header">
                    <div className="col-md-12 text-left">
                        <Link to='/home'><h1 className="logo">myspace</h1></Link>

                        {
                            (this.props.details)
                                ?

                                <div className="col-md-12 text-left" >
                                    {/* here goes employee image,details*/}
                                    <div className="emp_info">
                                        <img src={`data:image/jpeg;base64,${this.props.details.photo}`} alt="empimg" className="employeeimage" />
                                        <div className="details">
                                            <p className="empname">{this.props.details.name}<br />
                                                <a href="#" className="logout" onClick={this.DoLogout}> Logout</a>
                                                {/*&nbsp;|&nbsp; <a href="#" >Change Password</a> */}
                                            </p>
                                        </div>
                                        <div className="search">
                                            <input type="text" placeholder="Search Employee By..ID,NAME"
                                                size="30" name="searchString"
                                                ref="search"
                                                onKeyPress={(e) => this.handleKeySearch(e, this.state.searchString)}
                                                onChange={this.handleInputChange} />
                                            {/* <input type="submit" className="submitbtn" onClick={() => this.handleSubmit(this.state.searchString)} value="Search" /> */}
                                        </div>
                                    </div>
                                </div>
                                : <div></div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        details: state.profilereducer.details,
        isLoaded: state.profilereducer.isLoaded
    };
}
function mapDispatchToProps(dispatch) {
    return {
        profileaction: bindActionCreators(profileaction, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);