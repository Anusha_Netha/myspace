import React, { Component } from 'react';
import Header from '../Components/Header';
import SideNav from '../Components/SideNavigation';
import Body from '../Components/Body';
import Footer from '../Components/Footer';
import '../Assets/css/Home.css';
//component for Homepage
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            //Home page all the integration goes here
            <div className="bg">
                <Header />
                <div className="row">
                    <div className="col-md-3">
                        <SideNav />
                    </div>
                    <div className="col-md-9">
                    <Body />
                    </div>
                </div>
                <Footer />
            </div>
         );
    }
}
 
export default Home;