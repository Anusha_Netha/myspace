import React, { Component } from 'react';
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import SideNav from '../Components/SideNavigation';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import '../Assets/css/postrave.css';
import * as postraveaction from '../Store/Actions/postraveaction';
import { isFulfilled } from 'q';
// component for postrave details of an employee
class Postrave extends Component {

    //initialises data
    constructor(props) {
        super(props);
        this.state = {
            empid: ' ',
            description: '',
            ravecompliment: '',
            createdBy: localStorage.getItem('id'),
            data: [],
            empidError: '',
            descriptionError: '',
            ravecomplimentError: '',
            isSubmitted: false,
            Flag: true

        }

        //binds the data
        this.handleEmpIdChange = this.handleEmpIdChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleRaveComplimentChange = this.handleRaveComplimentChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount(){
        const token = localStorage.getItem('jwt-token');
        const old_auth_time = localStorage.getItem('auth-time');
        const new_auth_time = Date.now();
        const session = new_auth_time - old_auth_time;

        if (session > 360000) {
            localStorage.removeItem('jwt-token')
            window.location.href = '/'
        }
    }
    //whenever a data is changed this function is called
    handleEmpIdChange(event) {
        this.setState({ empid: event.target.value }, () => {
            this.validateEmpId()
        });
    }
    // whenever  a description changes this function changes
    handleDescriptionChange(event) {
        this.setState({ description: event.target.value }, () => {
            this.validateDescription()
        });
    }
    // whenever a compliment  changes this function changes
    handleRaveComplimentChange(event) {
        this.setState({ ravecompliment: event.target.value }, () => {
            this.validateRaveCompliment()
        })
    }
    //whenever it submits with click button with validations
    handleSubmit(event) {


        if (this.validateEmpId() && this.validateDescription() && this.validateRaveCompliment()) {
            // alert("it is correct")
            const id = localStorage.getItem('id');
            if(id !== this.state.empid){
            this.props.postraveaction.postRave(
                {
                    employeeId: this.state.empid,
                    description: this.state.description,
                    createdBy: this.state.createdBy,
                    raveCompliment: this.state.ravecompliment,
                }
            )
            this.setState({
                isSubmitted: true
            })
        }
        else{
            alert('You cannot Compliment yourself')
        }
        }
        else {
            alert("please enter valid details")
        }

    }

    // validating the EmployeeId field with validations
    validateEmpId = () => {
        const { empid } = this.state;
        let valid = false
        let empidCheck = 0;
        for (let i = 0; i < empid.length; i++) {
            if (empid.charAt(i) >= 48 || empid.charAt(i) <= 57) {
                empidCheck++;
            }
        }
        this.setState({
            empidError:
                empid.length == 5 && empidCheck == empid.length ? ' ' : 'length of employee id should be 5 and it should contain only numbers'
        })
        if (empidCheck == 5) {

            valid = true;

        }

        return valid;

    }

    // validating the Description field with validations
    validateDescription = () => {
        const { description } = this.state;
        let valid = false;
        this.setState({
            descriptionError:
                (description.length >= 5 && description.length <= 100) && (/^[a-zA-Z]*$/).test(description) || description.includes(" ") ? ' ' : 'description should be in between 5 to 100 and it should contain only letters'
        })
        if (this.state.descriptionError == ' ') {
            valid = true;
            return valid;
        }
    }
    // validating the Compliment field with validations
    validateRaveCompliment = () => {
        const { ravecompliment } = this.state;
        let valid = false;
        this.setState({
            ravecomplimentError:
                ravecompliment.length >= 5 && ravecompliment.length <= 50 && (/^[a-zA-Z]*$/).test(ravecompliment) || ravecompliment.includes(" ") ? ' ' : 'compliment should be on between  5 to 50 and it should contain only letters'
        })
        if (this.state.ravecomplimentError == ' ') {
            valid = true;
            return valid;
        }
    }
    render() {

        //based on result data is displayed
        if (this.props.data == 200 ) {
            alert("your data is submitted successfully")
        }
        if (this.props.data == 202 && this.state.isSubmitted) {
            alert("employee id does not exists")
            this.setState({
                isSubmitted: false
            })
        }
        if (this.props.data == 201 && this.state.isSubmitted) {
            alert("your post is not submitted")
            this.setState({
                isSubmitted: false
            })
        }

        return (

            //Form to post rave
            <div className="bg">
                <Header />
                <div className="row">
                    <div className="col-md-3">
                        <SideNav />
                    </div>
                    <div className="col-md-9">
                        {/* <Body /> */}
                        <div className="container text-left rave-form ">

                            <form >
                                <h2 className="text-center">RAVE</h2>
                                <div className="form-group ">
                                    <label for="empid">EmployeeID <span style={{ color: 'red' }}>*</span></label>
                                    <input type="text" name="empid"
                                        placeholder="Enter Employee ID"
                                        className="form-control"
                                        // className={`form-control ${this.state.empidError ? 'is-invalid' : ' '}`}
                                        onChange={this.handleEmpIdChange}

                                        required />
                                    <div style={{ color: "red", fontSize: '16px' }}>{this.state.empidError}</div>
                                </div>
                                <div className="form-group ">
                                    <label for="description">Description <span style={{ color: 'red' }}>*</span></label>
                                    <input type="text"
                                        name="description"
                                        placeholder="Give Description"
                                        className="form-control"
                                        // className={`form-control ${this.state.descriptionError ? 'is-invalid' : ' '}`}
                                        onChange={this.handleDescriptionChange}

                                        required />
                                    <div style={{ color: 'red', fontSize: '16px' }}>{this.state.descriptionError}</div>
                                </div>
                                <div className="form-group ">
                                    <label for="ravecompliment">Compliment <span style={{ color: 'red' }}>*</span></label>
                                    <input type="text"
                                        name="ravecompliment"
                                        placeholder="Enter RAVE Compliment"
                                        className="form-control"
                                        // className={`form-control ${this.state.ravecomplimentError ? 'is-invalid' : ' '}`}
                                        onChange={this.handleRaveComplimentChange}

                                        required />
                                    <div style={{ color: 'red', fontSize: '16px' }}>{this.state.ravecomplimentError}</div>
                                </div>
                                <div className="form-group " >
                                    <input type="submit" value="Submit" className="form-control btn" onClick={this.handleSubmit} />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        data: state.postravereducer.data,
        isLoaded: state.postravereducer.isLoaded

    };
}
function mapDispatchToProps(dispatch) {
    return {
        postraveaction: bindActionCreators(postraveaction, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Postrave);