import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as raveaction from '../Store/Actions/raveaction';
import '../Assets/css/Rave.css';
import Footer from '../Components/Footer';
import Header from '../Components/Header';
import SideNav from '../Components/SideNavigation';
import congrats from '../Assets/Images/congrats.jpeg';

class Rave extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    //this function retrieves data before render()
    componentWillMount() {
        const token = localStorage.getItem('jwt-token');
        const old_auth_time = localStorage.getItem('auth-time');
        const new_auth_time = Date.now();
        const session = new_auth_time - old_auth_time;

        if (session > 360000) {
            localStorage.removeItem('jwt-token')
            window.location.href = '/'
        }
        console.log("componentmounted")
        this.props.raveaction.fetchrave();

    }
    render() {
        return (
            //Displays all Raves
            <div>
                <div className="bg">
                    <Header />
                    <div className="row">
                        <div className="col-md-3">
                            <SideNav />
                        </div>
                        <div className="col-md-9">
                            {/* <Body /> */}
                            <div className="container">
                                <div>
                                    {(this.props.details) ?
                                        <div class="row">
                                            {this.props.details.map(item => (

                                                <div class="col-md-4 mt-4">
                                                    <div class="card mb-4">
                                                        <img class="card-img-top" src={congrats} alt="Card image cap" />
                                                        <div class="card-body text-left">
                                                            <h5 class="card-title">{item.employeeId}</h5>
                                                            <p class="card-text ">
                                                                Compliments :&nbsp;&nbsp;{item.raveCompliment}
                                                                <br></br>
                                                                Description :&nbsp;&nbsp;{item.description}
                                                                <br></br>
                                                                CreatedBy   {item.createdBy}
                                                                <br></br>
                                                                Postedon {item.createdAt}

                                                            </p>

                                                        </div>
                                                    </div>
                                                </div>



                                            ))}
                                        </div>
                                        : <div></div>}

                                </div>
                            </div>
                        </div>
                    </div>
                    <Footer />
                </div>

            </div>
        );
    }
}
function mapStateToProps(state) {
    debugger;
    console.log(state);
    return {
        details: state.ravereducer.details,
    };
}
function mapDispatchToProps(dispatch) {
    debugger;
    return {
        raveaction: bindActionCreators(raveaction, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Rave);