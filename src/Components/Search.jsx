import React, { Component } from 'react';
import Header from './Header';
import Footer from './Footer';
import SideNav from './SideNavigation'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as searchaction from '../Store/Actions/searchaction';
import * as searchnameaction from '../Store/Actions/searchbyname';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //intialises the values
            searchString: ' ',
            details: {},
            isSubmitted: false,
        }
    }

    //this function will fetch the data before render function
    componentWillMount() {

        const token = localStorage.getItem('jwt-token');
        const old_auth_time = localStorage.getItem('auth-time');
        const new_auth_time = Date.now();
        const session = new_auth_time - old_auth_time;

        if (session > 360000) {
            localStorage.removeItem('jwt-token')
            window.location.href = '/'
        }
        const searchString = this.props.match.params.value
        console.log(searchString);
        if (searchString.charAt(0) >= 0 && searchString.charAt(0) <= 9) {
            console.log("search")
            this.props.searchaction.searchemployeeId(searchString);
            this.setState({
                isSubmitted: true,
            })
        }
        else {
            this.props.searchnameaction.searchemployeeName(searchString);
            this.setState({
                isSubmitted: true,
            })
        }


    }

    //displays data
    render() {
        if (this.props.statuscode === 200 || this.props.status === 200) {
            if (this.props.statuscode == 200) {
                this.state.details = this.props.details;
            }
            else {
                this.state.details = this.props.searchdetails;
            }
            return (

                <div className="bg">
                    <Header />
                    <div className="row">
                        <div className="col-md-3">
                            <SideNav />
                        </div>
                        <div className="col-md-9">
                            {/* <Body /> */}
                            <div>
                                <div className="container">
                                    <div className="card searchemp">
                                        <i className="fa fa-user fa-8x text-center defaultemp" aria-hidden="true" ></i>
                                        {/* <img src={`data:image/jpeg;base64,${this.props.details.photo}`} alt="employeeimg" className="employeeimage" /> */}
                                        <hr />
                                        <div className="card-body text-left">
                                            <table >
                                                <tr className="card-text"><td>Name</td><td>:</td><td>{this.state.details.name}</td></tr>
                                                <tr className="card-text"><td>Employement Type</td><td>:</td><td>{this.state.details.employmentType}</td></tr>
                                                <tr className="card-text"><td>Designation</td><td>:</td><td>{this.state.details.designation}</td></tr>
                                                <tr className="card-text"><td>Department</td><td>:</td><td>{this.state.details.department}</td></tr>
                                                <tr className="card-text"><td>Practice</td><td>:</td><td>{this.state.details.practice}</td></tr>
                                                <tr className="card-text"><td>Work Location</td><td>:</td><td>{this.state.details.workLocation}</td></tr>
                                                <tr className="card-text"><td>Company Email-id</td><td>:</td><td>{this.state.details.companyEmailId}</td></tr>
                                                <tr className="card-text"><td>Permanent Location</td><td>:</td><td>{this.state.details.permanentLocation}</td></tr>
                                                <tr className="card-text"><td>WorkStation ID</td><td>:</td><td>{this.state.details.workStationId}</td></tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <Footer />
                </div>
            );
        }
        else {
            if (this.props.statuscode == 204 && this.state.isSubmitted) {
                // window.location.href="/home"

                if (this.state.isSubmitted) {

                    alert("there is no such employee ");
                    this.setState({ isSubmitted: false });

                }
            }

            else if (this.props.status == 204 && this.state.isSubmitted) {
                // window.location.href="/home"

                if (this.state.isSubmitted) {

                    alert("there is no such employee ");
                    this.setState({ isSubmitted: false });


                }
            }
            return (
                <div>
                    <div className="bg">
                        <Header />
                        <div className="row">
                            <div className="col-md-3">
                                <SideNav />
                            </div>
                            <div className="col-md-9">
                                <h1>No Employees Found</h1>
                            </div>
                        </div>
                        <Footer />
                    </div>
                </div>
            );
        }


    }
}
function mapStateToProps(state) {
    debugger;
    console.log(state);

    return {
        details: state.searchreducer.details,
        isLoaded: state.searchreducer.isLoaded,
        statuscode: state.searchreducer.statuscode,
        searchdetails: state.searchnamereducer.details,
        status: state.searchnamereducer.statuscode,
    }
}
function mapDispatchToProps(dispatch) {
    debugger;
    return {
        searchaction: bindActionCreators(searchaction, dispatch),
        searchnameaction: bindActionCreators(searchnameaction, dispatch),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Search);