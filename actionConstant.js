/**
 * Action cOnmstants for the login page
 */
export const DO_LOGIN_USER = 'DO_LOGIN_USER';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_DATA_ERROR='LOGIN_USER_DATA_ERROR';

/**
 * Action Constants for Profile View
 */

export const FETCH_PROFILE_BY_ID='FETCH_PROFILE_BY_ID';
export const RECEIEVE_PROFILE_DETAILS_BY_ID='RECEIEVE_PROFILE_DETAILS_BY_ID';
export const UPDATE_VALUES ='UPDATE_VALUES';
export const  UPDATED_SUCCESSFULL = 'UPDATED_SUCCESSFULL';

/**
 * action constants for Search Employee by id
 */
export const SEARCH_EMPLOYEE_BY_ID = 'SEARCH_EMPLOYEE_BY_ID';
export const RECEIEVE_EMPLOYEE_DETAILS ='RECEIEVE_EMPLOYEE_DETAILS';
export const SEARCH_EMPLOYEE_BY_NAME = 'SEARCH_EMPLOYEE_BY_NAME';
export const RECEIEVE_EMPLOYEE_BY_NAME='RECEIEVE_EMPLOYEE_BY_NAME';
export  const  SEARCH_NOT_EXISTS = 'SEARCH_NOT_EXISTS';

/**
 * action constants for Rave
 */
export const RAVE_ALL = 'RAVE_ALL';
export const RECEIVE_RAVE_ALL ="RECEIVE_RAVE_ALL";
export const POSTRAVE = 'POSTRAVE';
export const POSTRAVESUCCESS = 'POSTRAVESUCCESS';
export const POST_RAVE_DATA_ERROR='POST_RAVE_DATA_ERROR';

/**
 * action constants fro drives
 */
export const DRIVE_EXPORT= 'DRIVE_EXPORT';
export const RECEIVE_DRIVE_ALL ="RECEIVE_DRIVE_ALL";
export const DRIVE_DATA_ERROR = "DRIVE_DATA_ERROR"

/**
 * action constants for home page
 */

export const NOTIFICATION = "NOTIFICATION";
export const RECEIVE_NOTIFICATION = "RECEIVE_NOTIFICATION";

/**
 * action constants for appraisal
 */
export const APPRAISAL_ALL = 'APPRAISAL_ALL';
export const RECEIVE_APPRAISAL_ALL ="RECEIVE_APPRAISAL_ALL";
export const POSTAPPRAISAL = 'POSTAPPRAISAL';
export const POSTAPPRAISALSUCCESS = 'POSTAPPRAISALSUCCESS';
export const POST_APPRAISAL_DATA_ERROR='POST_APPRAISAL_DATA_ERROR';


/**
 * action constants for Query forum
 */

 export const GET_ALL_ANSWERS = `GET_ALL_ANSWERS`;
 export const RECEIVED_ANSWERS = `RECEIVED_ANSWERS`;
 /**
  * 
  */
 export const POSTANSWER = 'POSTANSWER ';
export const POSTANSWERSUCCESS = 'POSTANSWERSUCCESS';
export const POST_ANSWER_DATA_ERROR='POST_ANSWER_DATA_ERROR';